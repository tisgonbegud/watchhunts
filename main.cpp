#include "WatchHunts.h"
#include "dbManager.h"
#include <QApplication>
#include <QDebug>


int main(int argc, char *argv[])
{
    qDebug()<<"Start!"; 

    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/app-icon.png"));

    WatchHunts w;
    w.show();
    qDebug()<<"End!";

    return a.exec();
}
