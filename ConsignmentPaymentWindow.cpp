#include "ConsignmentPaymentWindow.h"
#include "ui_ConsignmentPaymentWindow.h"

ConsignmentPaymentWindow::ConsignmentPaymentWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConsignmentPaymentWindow)
{
    ui->setupUi(this);
}

ConsignmentPaymentWindow::~ConsignmentPaymentWindow()
{
    delete ui;
}
