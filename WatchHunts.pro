#-------------------------------------------------
#
# Project created by QtCreator 2019-09-05T17:37:17
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WatchHunts
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        ConsignmentEditWindow.cpp \
        ConsignmentWindow.cpp \
        CustomerSearch.cpp \
        ProductSearch.cpp \
        PurchaseWindow.cpp \
        SaleWindow.cpp \
        ViewDatabase.cpp \
        dbManager.cpp \
        main.cpp \
        WatchHunts.cpp

HEADERS += \
        ConsignmentEditWindow.h \
        ConsignmentWindow.h \
        CustomerSearch.h \
        ProductSearch.h \
        PurchaseWindow.h \
        SaleWindow.h \
        ViewDatabase.h \
        WatchHunts.h \
        dbManager.h

FORMS += \
        ConsignmentEditWindow.ui \
        ConsignmentWindow.ui \
        CustomerSearch.ui \
        ProductSearch.ui \
        PurchaseWindow.ui \
        SaleWindow.ui \
        ViewDatabase.ui \
        WatchHunts.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources/watchhunts.qrc \
