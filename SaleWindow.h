#ifndef SALEWINDOW_H
#define SALEWINDOW_H

#include <QDialog>
#include <QDate>
#include <QKeyEvent>
#include <QMessageBox>
#include <QPixmap>
#include <QRegExpValidator>
#include "dbManager.h"
#include "CustomerSearch.h"
#include "ProductSearch.h"

namespace Ui {
class SaleWindow;
}

class SaleWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SaleWindow(QWidget *parent = nullptr);
    ~SaleWindow();

private slots:
    void on_PaymentModePicker_currentTextChanged(const QString &arg1);

    void on_OkButton_clicked();

    void on_CustomerClearButton_clicked();

    void on_AddNewCustomerButton_clicked();

    void on_CustomerSearchButton_clicked();

    void on_LineName_textChanged(const QString &arg1);

    void on_LineContactNo_textChanged(const QString &arg1);

    void on_LineNRIC_textChanged(const QString &arg1);

    void on_TextAddress_textChanged();

    void on_BackButton_clicked();

    void keyPressEvent(QKeyEvent *e);

    void on_ProductSearchButton_clicked();

    void on_ProductClearButton_clicked();

    void on_LinePrice1_textChanged(const QString &arg1);

    void on_LinePrice2_textChanged(const QString &arg1);

    void on_LinePrice3_textChanged(const QString &arg1);

    void on_LinePrice4_textChanged(const QString &arg1);

    void on_LinePrice5_textChanged(const QString &arg1);

    void on_LineRemarks1_textChanged(const QString &arg1);

    void on_LineRemarks2_textChanged(const QString &arg1);

    void on_LineRemarks3_textChanged(const QString &arg1);

    void on_LineRemarks4_textChanged(const QString &arg1);

    void on_LineRemarks5_textChanged(const QString &arg1);

    void on_EditButton_clicked();

private:
    Ui::SaleWindow *ui;

    void productClearButton_enabler(const QString &arg1);

    void executeRecordTransaction();

    dbManager *d_db;

    CustomerSearch *customerSearchWin;

    ProductSearch *productSearchWin;

};

#endif // SALEWINDOW_H
