#include "ViewDatabase.h"
#include "ui_ViewDatabase.h"

ViewDatabase::ViewDatabase(const QString& dataType, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ViewDatabase)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::Dialog);
    this->setFixedSize(QSize(1080, 460));

    //install event filter to disable close by Esc Key (remember to include header QKeyEvent header file)
    this->installEventFilter(this);

    //create database object to access data
    //first get the working directory of the executable then assign the database to a directory in database folder
    QString currentWd = QDir::currentPath();
    QString databaseWd = currentWd + "/database/db.sqlite";
    d_db = new dbManager(databaseWd);

    if (dataType == "Stock")
    {
        viewStockData();
    }
    else if (dataType == "Customers")
    {
        viewCustomerData();
    }
    else if (dataType == "Consignment")
    {
        viewConsignmentData();
    }
    else if (dataType == "Sale Transactions")
    {
        viewSaleTransactions();
    }
    else if (dataType == "Purchase Transactions")
    {
        viewPurchaseTransactions();
    }
}

ViewDatabase::~ViewDatabase()
{
    delete ui;
}

void ViewDatabase::on_CloseButton_clicked()
{
    this->reject();
}

void ViewDatabase::viewCustomerData()
{
    //Displays all customers data in the beginning
    modal = new QSqlTableModel();
    QString sqlStatement = "SELECT name, contact_no, nric_no, address FROM customersTable;";
    QSqlQuery d_qry;

    d_qry.prepare(sqlStatement);
    if (!d_qry.exec())
    {
        qDebug()<<d_qry.lastError();
    }

    int widthOffset = 500;
    this->setFixedSize(QSize(1080 - widthOffset, 460));
    ui->TableData->setGeometry(ui->TableData->x(), ui->TableData->y(), ui->TableData->width() - widthOffset, ui->TableData->height());
    this->setWindowTitle("View Customer Data");

    modal->setQuery(d_qry);
    modal->setHeaderData(0, Qt::Horizontal, tr("Name"));
    modal->setHeaderData(1, Qt::Horizontal, tr("Contact No"));
    modal->setHeaderData(2, Qt::Horizontal, tr("NRIC/Passport No"));
    modal->setHeaderData(3, Qt::Horizontal, tr("Address"));

    tableModel = ui->TableData;

    tableModel->setModel(modal);
    tableModel->resizeColumnsToContents();
    tableModel->horizontalHeader()->setHighlightSections(false);
    tableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    tableModel->horizontalHeader()->setStretchLastSection(true);
    tableModel->horizontalHeader()->setMinimumSectionSize(100);

}

void ViewDatabase::viewStockData()
{
    //Displays all stock data in the beginning
    modal = new QSqlTableModel();
    QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no, cert_details, cost_price, selling_price, date_purchased, 'P-' || printf('%03d', purchaseinvoice_id), purchase_cheque_no, date_sold, NULLIF('S-' || printf('%03d', saleinvoice_id), 'S-000') FROM stockOverviewTable LEFT JOIN purchaseTxnInvoicetable USING (purchasetxn_id) LEFT JOIN purchaseInvoiceTable USING (purchaseinvoice_id) LEFT JOIN purchasetxnTable USING (purchasetxn_id) LEFT JOIN productTable USING (product_id) LEFT JOIN saletxninvoicetable USING (saletxn_id) LEFT JOIN saleinvoicetable USING (saleinvoice_id) LEFT JOIN saleTxnTable USING (saletxn_id);";
    QSqlQuery d_qry;

    d_qry.prepare(sqlStatement);
    if (!d_qry.exec())
    {
        qDebug()<<d_qry.lastError();
    }

    this->setWindowTitle("View Stock Data");

    modal->setQuery(d_qry);
    modal->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
    modal->setHeaderData(1, Qt::Horizontal, tr("Brand"));
    modal->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
    modal->setHeaderData(3, Qt::Horizontal, tr("Model No"));
    modal->setHeaderData(4, Qt::Horizontal, tr("Serial No"));
    modal->setHeaderData(5, Qt::Horizontal, tr("Certificate Details"));
    modal->setHeaderData(6, Qt::Horizontal, tr("Cost Price"));
    modal->setHeaderData(7, Qt::Horizontal, tr("Selling Price"));
    modal->setHeaderData(8, Qt::Horizontal, tr("Date Purchased"));
    modal->setHeaderData(9, Qt::Horizontal, tr("Purchase Invoice"));
    modal->setHeaderData(10, Qt::Horizontal, tr("Purchase Cheque No"));
    modal->setHeaderData(11, Qt::Horizontal, tr("Date Sold"));
    modal->setHeaderData(12, Qt::Horizontal, tr("Sale Invoice"));

    tableModel = ui->TableData;

    tableModel->setModel(modal);
    tableModel->resizeColumnsToContents();
    tableModel->horizontalHeader()->setHighlightSections(false);
    tableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    tableModel->horizontalHeader()->setStretchLastSection(true);
    tableModel->horizontalHeader()->setMinimumSectionSize(50);
}

void ViewDatabase::viewConsignmentData()
{
    //Displays all stock data in the beginning
    modal = new QSqlTableModel();
    QString sqlStatement = "SELECT 'C' || printf('%03d', consignment_id), hrc_id, brand_name, model_name, model_no, serial_no, cert_details, hrc_cost, date_consigned, consignmentinvoice_id, selling_price, date_sold, NULLIF('S-' || printf('%03d', saleinvoice_id), 'S-000') FROM consignmentOverviewTable LEFT JOIN consignmenttxnTable USING (consignmenttxn_id) LEFT JOIN consignmenttxnInvoiceTable USING (consignmenttxn_id) LEFT JOIN productTable USING (product_id) LEFT JOIN saleTxnTable USING (saletxn_id) LEFT JOIN saleTxnInvoiceTable USING (saletxn_id)";
    QSqlQuery d_qry;

    d_qry.prepare(sqlStatement);
    if (!d_qry.exec())
    {
        qDebug()<<d_qry.lastError();
    }

    this->setWindowTitle("View Consignment Data");

    modal->setQuery(d_qry);
    modal->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
    modal->setHeaderData(1, Qt::Horizontal, tr("HRC ID"));
    modal->setHeaderData(2, Qt::Horizontal, tr("Brand"));
    modal->setHeaderData(3, Qt::Horizontal, tr("Model Name"));
    modal->setHeaderData(4, Qt::Horizontal, tr("Model No"));
    modal->setHeaderData(5, Qt::Horizontal, tr("Serial No"));
    modal->setHeaderData(6, Qt::Horizontal, tr("Certificate Details"));
    modal->setHeaderData(7, Qt::Horizontal, tr("HRC Cost"));
    modal->setHeaderData(8, Qt::Horizontal, tr("Date Consigned"));
    modal->setHeaderData(9, Qt::Horizontal, tr("Consignment Invoice"));
    modal->setHeaderData(10, Qt::Horizontal, tr("Selling Price"));
    modal->setHeaderData(11, Qt::Horizontal, tr("Date Sold"));
    modal->setHeaderData(12, Qt::Horizontal, tr("Sale Invoice"));

    tableModel = ui->TableData;

    tableModel->setModel(modal);
    tableModel->resizeColumnsToContents();
    tableModel->horizontalHeader()->setHighlightSections(false);
    tableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    tableModel->horizontalHeader()->setStretchLastSection(true);
    tableModel->horizontalHeader()->setMinimumSectionSize(50);
}


void ViewDatabase::viewSaleTransactions()
{
    //Displays all sale transaction during initialization
    modal = new QSqlTableModel();
    QString sqlStatement = "SELECT NULLIF('S-' || printf('%03d', saleinvoice_id), 'S-000'), name, contact_no, nric_no, address, NULLIF('WH' || printf('%03d', stock_id), 'WH000'), brand_name, model_name, model_no, serial_no, date_sold, sale_remarks, selling_price, sale_paymentmethod, sale_cheque_no, last3_4digits FROM saleInvoiceTable LEFT JOIN saleTxnInvoiceTable USING (saleinvoice_id) LEFT JOIN saleTxnTable USING (saleTxn_id) LEFT JOIN customersTable ON saleTxnTable.sale_customer_id = customersTable.customer_id LEFT JOIN productTable USING (product_id) LEFT JOIN stockOverviewTable USING (saletxn_id) LEFT JOIN consignmentOverviewTable USING (saleTxn_id) WHERE stock_id IS NOT NULL UNION SELECT NULLIF('S-' || printf('%03d', saleinvoice_id), 'S-000'), name, contact_no, nric_no, address, NULLIF('C' || printf('%03d', consignment_id), 'C000'), brand_name, model_name, model_no, serial_no, date_sold, sale_remarks, selling_price, sale_paymentmethod, sale_cheque_no, last3_4digits FROM saleInvoiceTable LEFT JOIN saleTxnInvoiceTable USING (saleinvoice_id) LEFT JOIN saleTxnTable USING (saleTxn_id) LEFT JOIN customersTable ON saleTxnTable.sale_customer_id = customersTable.customer_id LEFT JOIN productTable USING (product_id) LEFT JOIN stockOverviewTable USING (saletxn_id) LEFT JOIN consignmentOverviewTable USING (saleTxn_id) WHERE consignment_id IS NOT NULL;";
    QSqlQuery d_qry;

    d_qry.prepare(sqlStatement);
    if (!d_qry.exec())
    {
        qDebug()<<d_qry.lastError();
    }

    this->setWindowTitle("View Sales Transactions");

    modal->setQuery(d_qry);
    modal->setHeaderData(0, Qt::Horizontal, tr("Sale Invoice No"));
    modal->setHeaderData(1, Qt::Horizontal, tr("Customer Name"));
    modal->setHeaderData(2, Qt::Horizontal, tr("Contact No"));
    modal->setHeaderData(3, Qt::Horizontal, tr("NRIC/Passport No"));
    modal->setHeaderData(4, Qt::Horizontal, tr("Address"));
    modal->setHeaderData(5, Qt::Horizontal, tr("WH ID"));
    modal->setHeaderData(6, Qt::Horizontal, tr("Brand"));
    modal->setHeaderData(7, Qt::Horizontal, tr("Model Name"));
    modal->setHeaderData(8, Qt::Horizontal, tr("Model No"));
    modal->setHeaderData(9, Qt::Horizontal, tr("Serial No"));
    modal->setHeaderData(10, Qt::Horizontal, tr("Date Sold"));
    modal->setHeaderData(11, Qt::Horizontal, tr("Remarks"));
    modal->setHeaderData(12, Qt::Horizontal, tr("Selling Price"));
    modal->setHeaderData(13, Qt::Horizontal, tr("Payment Method"));
    modal->setHeaderData(14, Qt::Horizontal, tr("Cheque No"));
    modal->setHeaderData(15, Qt::Horizontal, tr("Last 3 or 4 digits"));

    tableModel = ui->TableData;

    tableModel->setModel(modal);
    tableModel->resizeColumnsToContents();
    tableModel->horizontalHeader()->setHighlightSections(false);
    tableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    tableModel->horizontalHeader()->setStretchLastSection(true);
    tableModel->horizontalHeader()->setMinimumSectionSize(50);
}

void ViewDatabase::viewPurchaseTransactions()
{
    //Displays all purchase transaction during initialization
    modal = new QSqlTableModel();
    QString sqlStatement = "SELECT 'P-' || printf('%03d', purchaseinvoice_id), name, contact_no, nric_no, address, 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no, date_purchased, purchase_remarks, cost_price, purchase_paymentmethod, purchase_cheque_no, date_sold, NULLIF('S-' || printf('%03d', saleinvoice_id), 'S-000') FROM stockOverviewTable LEFT JOIN purchasetxnTable USING (purchasetxn_id) LEFT JOIN purchaseTxnInvoiceTable USING (purchasetxn_id) LEFT JOIN customersTable ON purchasetxnTable.purchase_customer_id = customersTable.customer_id LEFT JOIN productTable USING (product_id) LEFT JOIN saleTxnTable USING (saletxn_id) LEFT JOIN saleTxnInvoiceTable USING (saletxn_id)";
    QSqlQuery d_qry;

    d_qry.prepare(sqlStatement);
    if (!d_qry.exec())
    {
        qDebug()<<d_qry.lastError();
    }

    this->setWindowTitle("View Purchase Transactions");

    modal->setQuery(d_qry);
    modal->setHeaderData(0, Qt::Horizontal, tr("Purchase Invoice No"));
    modal->setHeaderData(1, Qt::Horizontal, tr("Customer Name"));
    modal->setHeaderData(2, Qt::Horizontal, tr("Contact No"));
    modal->setHeaderData(3, Qt::Horizontal, tr("NRIC/Passport No"));
    modal->setHeaderData(4, Qt::Horizontal, tr("Address"));
    modal->setHeaderData(5, Qt::Horizontal, tr("WH ID"));
    modal->setHeaderData(6, Qt::Horizontal, tr("Brand"));
    modal->setHeaderData(7, Qt::Horizontal, tr("Model Name"));
    modal->setHeaderData(8, Qt::Horizontal, tr("Model No"));
    modal->setHeaderData(9, Qt::Horizontal, tr("Serial No"));
    modal->setHeaderData(10, Qt::Horizontal, tr("Date Purchased"));
    modal->setHeaderData(11, Qt::Horizontal, tr("Remarks"));
    modal->setHeaderData(12, Qt::Horizontal, tr("Cost Price"));
    modal->setHeaderData(13, Qt::Horizontal, tr("Payment Method"));
    modal->setHeaderData(14, Qt::Horizontal, tr("Cheque No"));
    modal->setHeaderData(15, Qt::Horizontal, tr("Date Sold"));
    modal->setHeaderData(16, Qt::Horizontal, tr("Sale Invoice No"));

    tableModel = ui->TableData;

    tableModel->setModel(modal);
    tableModel->resizeColumnsToContents();
    tableModel->horizontalHeader()->setHighlightSections(false);
    tableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    tableModel->horizontalHeader()->setStretchLastSection(true);
    tableModel->horizontalHeader()->setMinimumSectionSize(50);
}
