#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QtCore>
#include <QtSql>


class dbManager
{

public:
    //constructor this class takes in a QString database directory as a parameter
    dbManager(const QString& dbPath);

    //common functions for sales and purchase
    void createCustomerTable();
    void addNewCustomer(const QString& name, const QString& contact_no,
                        const QString& nric_no, const QString& address);

    void createProductTable();
    void addNewProduct(const QString& brand_name,
                       const QString& model_name, const QString& model_no,
                       const QString& serial_no, const QString& cert_details);

    //purchase function
    void createPurchaseTxnTable();
    void addPurchaseTxn(const float& cost_price, const QString& date_purchased, const QString& purchase_paymentmethod,
                        const QString& purchase_cheque_no, const QString& purchase_remarks,
                        const int& purchase_customer_id, const int& product_id);

    void createPurchaseInvoiceTable();
    void addPurchaseInvoice(const QString& date);

    void createPurchaseTxnInvoiceTable();
    void addPurchaseTxnInvoice(const int& purchaseinvoice_id);

    //sales function
    void createSaleTxnTable();
    void addSaleTxn(const float& selling_price, const QString& date_sold, const QString& sale_paymentmethod,
                    const QString& sale_cheque_no, const QString& last3_4digits, const QString& sale_remarks,
                    const int& sale_customer_id, const int& product_id);

    void createSaleInvoiceTable();
    void addSaleInvoice(const QString& date);

    void createSaleTxnInvoiceTable();
    void addSaleTxnInvoice(const int& saleinvoice_id);

    //consignment function
    void createConsignmentTxnTable();
    void addConsignmentTxn(const QString& hrc_id, const int& product_id,
                           const QString& hrc_cost, const QString& date_consigned);

    void createConsignmentInvoiceTable();
    void addConsignmentInvoice(const QString& date_consigned);

    void createConsignmentTxnInvoiceTable();
    void addConsignmentTxnInvoice(const int& consignmentinvoice_id);

    void createConsignmentPaymentTxnTable();

    //overview function
    void createStockOverviewTable();
    void addPurchaseStockOverview(const int& purchasetxn_id);
    void addSaleStockOverview(const int& saletxn_id, const int& stock_id);
    void addSaleConsignmentOverview(const int& saletxn_id, const int& consignment_id);

    void createConsignmentOverviewTable();
    void addConsignmentOverview(const int& consignmenttxn_id);

private:
    QSqlDatabase m_db;

};

#endif // DBMANAGER_H
