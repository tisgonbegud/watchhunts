#include "WatchHunts.h"
#include "ui_WatchHunts.h"


WatchHunts::WatchHunts(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WatchHunts)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    this->setFixedSize(QSize(370, 485));

    //Add list of items that users can search in the past transaction section
    QStringList transactionType = QStringList()<<"Sales"<<"Purchases"<<"Consignment";
    transactionPicker = ui->TransactionTypePicker;
    transactionPicker->addItems(transactionType);

    QStringList yearList = QStringList()<<"All";
    transactionYearPicker = ui->TransactionYearPicker;
    transactionYearPicker->addItems(yearList);

    //first get the working directory of the executable then assign the database to a directory in database folder
    QString currentWd = QDir::currentPath();
    if (!QDir("database").exists())
    {
        QDir().mkdir("database");
    }
    QString databaseWd = currentWd + "/database/db.sqlite";

    w_db = new dbManager(databaseWd);
    createAndConnectToDatabase();
}

WatchHunts::~WatchHunts()
{
    delete ui;
}

void WatchHunts::on_NewSalesButton_clicked()
{
    this->hide();
    saleWin = new SaleWindow();
    saleWin->setModal(true);
    saleWin->exec();
    this->show();
}

void WatchHunts::on_NewPurchaseButton_clicked()
{
    this->hide();
    purchaseWin = new PurchaseWindow();
    purchaseWin->setModal(true);
    purchaseWin->exec();
    this->show();

}

void WatchHunts::on_ConsignmentButton_clicked()
{
    this->hide();
    consignmentWin = new ConsignmentWindow();
    consignmentWin->setModal(true);
    consignmentWin->exec();
    this->show();
}

void WatchHunts::on_ViewStockButton_clicked()
{
    this->hide();
    databaseWin = new ViewDatabase("Stock");
    databaseWin->setModal(true);
    databaseWin->exec();
    this->show();

}

void WatchHunts::on_ViewCustomerButton_clicked()
{
    this->hide();
    databaseWin = new ViewDatabase("Customers");
    databaseWin->setModal(true);
    databaseWin->exec();
    this->show();
}

void WatchHunts::on_ViewConsignmentButton_clicked()
{
    this->hide();
    databaseWin = new ViewDatabase("Consignment");
    databaseWin->setModal(true);
    databaseWin->exec();
    this->show();
}

void WatchHunts::on_TransactionOkButton_clicked()
{
    if (transactionPicker->currentText() == "Sales" && transactionYearPicker->currentText() == "All")
    {
        this->hide();
        databaseWin = new ViewDatabase("Sale Transactions");
        databaseWin->setModal(true);
        databaseWin->exec();
        this->show();
    }
    else if (transactionPicker->currentText() == "Purchases" && transactionYearPicker->currentText() == "All")
    {
        this->hide();
        databaseWin = new ViewDatabase("Purchase Transactions");
        databaseWin->setModal(true);
        databaseWin->exec();
        this->show();
    }
}

void WatchHunts::createAndConnectToDatabase()
{
    //initialize all the necessary tables
    w_db->createCustomerTable();    //common table
    w_db->createProductTable();

    w_db->createPurchaseTxnTable(); //purchase tables
    w_db->createPurchaseInvoiceTable();
    w_db->createPurchaseTxnInvoiceTable();

    w_db->createSaleTxnTable(); //sale tables
    w_db->createSaleInvoiceTable();
    w_db->createSaleTxnInvoiceTable();

    w_db->createConsignmentTxnTable(); //consignment tables
    w_db->createConsignmentInvoiceTable();
    w_db->createConsignmentTxnInvoiceTable();

    w_db->createConsignmentPaymentTxnTable();

    w_db->createConsignmentOverviewTable();
    w_db->createStockOverviewTable(); //common table
}
