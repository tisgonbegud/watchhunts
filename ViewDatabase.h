#ifndef VIEWDATABASE_H
#define VIEWDATABASE_H

#include <QDialog>
#include <QTableView>
#include <QMessageBox>
#include <QKeyEvent>
#include "dbManager.h"

namespace Ui {
class ViewDatabase;
}

class ViewDatabase : public QDialog
{
    Q_OBJECT

public:
    explicit ViewDatabase(const QString &dataType, QWidget *parent = nullptr);
    ~ViewDatabase();

private slots:
    void on_CloseButton_clicked();

private:
    Ui::ViewDatabase *ui;

    dbManager *d_db;

    QSqlQueryModel *modal;

    QTableView *tableModel;

    void viewCustomerData();
    void viewStockData();
    void viewConsignmentData();

    void viewSaleTransactions();
    void viewPurchaseTransactions();
    void viewConsignmentTransactions();

};

#endif // VIEWDATABASE_H
