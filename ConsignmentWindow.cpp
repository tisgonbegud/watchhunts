#include "ConsignmentWindow.h"
#include "ui_ConsignmentWindow.h"

ConsignmentWindow::ConsignmentWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConsignmentWindow)
{
    ui->setupUi(this);

    //set the window flag
    this->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::Dialog);

    //create database object to access data
    //first get the working directory of the executable then assign the database to a directory in database folder
    QString currentWd = QDir::currentPath();
    QString databaseWd = currentWd + "/database/db.sqlite";
    d_db = new dbManager(databaseWd);

    //TODO set the windows fixed size to prevent unauthorised resize

    //set the date picker to be today's date
    ui->ValueDate->setDate(QDate::currentDate());

    //Setting table widget object
    tableReceive = ui->TableReceive;
    tableUpdate = ui->TableUpdate;

    //Consignment receipt section
    //Create model with the desired rows
    receiveModel = new QStandardItemModel(1, 7, this);
    receiveModel->setHeaderData(0, Qt::Horizontal, "HRC ID");
    receiveModel->setHeaderData(1, Qt::Horizontal, "Brand");
    receiveModel->setHeaderData(2, Qt::Horizontal, "Model Name");
    receiveModel->setHeaderData(3, Qt::Horizontal, "Model No");
    receiveModel->setHeaderData(4, Qt::Horizontal, "Serial No");
    receiveModel->setHeaderData(5, Qt::Horizontal, "Certificate Details");
    receiveModel->setHeaderData(6, Qt::Horizontal, "HRC Cost Price");

    //set the default row counter
    ui->ValueRowCount->setText(QString::number(receiveModel->rowCount()));

    tableReceive->setModel(receiveModel);
    tableReceive->resizeColumnsToContents();
    tableReceive->resizeRowsToContents();
    tableReceive->horizontalHeader()->setHighlightSections(false);
    tableReceive->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    tableReceive->horizontalHeader()->setStretchLastSection(true);
    tableReceive->horizontalHeader()->setMinimumSectionSize(70);

    tableReceive->verticalHeader()->setVisible(true);
    tableReceive->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    //Consignment Update section
    //Create model with the desired rows
    updateModel = new QSqlQueryModel();
    QSqlQuery d_qry;
    QString sqlStatement = "SELECT NULLIF('C' || printf('%03d', consignment_id), 'C000'), brand_name, model_name, model_no, serial_no, cert_details, date_sold, NULLIF('S-' || printf('%03d', saleinvoice_id), 'S-000') FROM consignmentOverviewTable LEFT JOIN consignmentTxnTable USING (consignmenttxn_id) LEFT JOIN productTable USING (product_id) LEFT JOIN saleTxnTable USING (saleTxn_id) LEFT JOIN saleTxnInvoiceTable USING (saleTxn_id) WHERE saleinvoice_id IS NOT NULL AND consignmentpaymenttxn_id IS NULL;";

    d_qry.prepare(sqlStatement);
    d_qry.exec();

    updateModel->setQuery(d_qry);

    updateModel->setHeaderData(0, Qt::Horizontal, "WH ID");
    updateModel->setHeaderData(1, Qt::Horizontal, "Brand");
    updateModel->setHeaderData(2, Qt::Horizontal, "Model Name");
    updateModel->setHeaderData(3, Qt::Horizontal, "Model No");
    updateModel->setHeaderData(4, Qt::Horizontal, "Serial No");
    updateModel->setHeaderData(5, Qt::Horizontal, "Certificate Details");
    updateModel->setHeaderData(6, Qt::Horizontal, "Date Sold");
    updateModel->setHeaderData(7, Qt::Horizontal, "Sale Invoice No");

    tableUpdate->setModel(updateModel);
    tableUpdate->resizeColumnsToContents();
    tableUpdate->resizeRowsToContents();
    tableUpdate->horizontalHeader()->setHighlightSections(false);
    tableUpdate->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    tableUpdate->horizontalHeader()->setStretchLastSection(true);
    tableUpdate->horizontalHeader()->setMinimumSectionSize(70);

    tableUpdate->verticalHeader()->setVisible(true);
    tableUpdate->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    ui->ValueConsignmentPendingPayment->setText(QString::number(updateModel->rowCount()));
}

ConsignmentWindow::~ConsignmentWindow()
{
    delete ui;
}
//TODO implement copy and paste events
void ConsignmentWindow::keyPressEvent(QKeyEvent *e)
{
    QItemSelectionModel* selection = tableReceive->selectionModel();
    QModelIndexList index = selection->selectedIndexes();
    if (e->matches(QKeySequence::Copy))
    {

    }
    else if (e->matches(QKeySequence::Paste))
    {

    }
}

void ConsignmentWindow::addRow()
{
    if (receiveModel->rowCount() <= 4)
    {
        int verticalOffset = 24;
        QPushButton *cancelButton = ui->CancelButton;
        QPushButton *okButton = ui->OkButton;
        QWidget* tabWidget = ui->tabWidget;

        tabWidget->setGeometry(tabWidget->x(), tabWidget->y(), tabWidget->width(), tabWidget->height() + verticalOffset);
        tableReceive->setGeometry(tableReceive->x(), tableReceive->y(), tableReceive->width(), tableReceive->height() + verticalOffset);
        cancelButton->setGeometry(cancelButton->x(), cancelButton->y() + verticalOffset, cancelButton->width(), cancelButton->height());
        okButton->setGeometry(okButton->x(), okButton->y() + verticalOffset, okButton->width(), okButton->height());

        ui->LabelRowCount->setGeometry(ui->LabelRowCount->x(), ui->LabelRowCount->y() + verticalOffset, ui->LabelRowCount->width(), ui->LabelRowCount->height());
        ui->ValueRowCount->setGeometry(ui->ValueRowCount->x(), ui->ValueRowCount->y() + verticalOffset, ui->ValueRowCount->width(), ui->ValueRowCount->height());

        this->setFixedSize(this->width(), this->height() + verticalOffset);
    }

    receiveModel->insertRow(receiveModel->rowCount());
    ui->ValueRowCount->setText(QString::number(receiveModel->rowCount()));
}

void ConsignmentWindow::removeRow()
{
    if(receiveModel->rowCount() <= 5)
    {
    int verticalOffset = 24;
    QPushButton *cancelButton = ui->CancelButton;
    QPushButton *okButton = ui->OkButton;
    QWidget* tabWidget = ui->tabWidget;

    tabWidget->setGeometry(tabWidget->x(), tabWidget->y(), tabWidget->width(), tabWidget->height() - verticalOffset);
    tableReceive->setGeometry(tableReceive->x(), tableReceive->y(), tableReceive->width(), tableReceive->height() - verticalOffset);
    cancelButton->setGeometry(cancelButton->x(), cancelButton->y() - verticalOffset, cancelButton->width(), cancelButton->height());
    okButton->setGeometry(okButton->x(), okButton->y() - verticalOffset, okButton->width(), okButton->height());

    ui->LabelRowCount->setGeometry(ui->LabelRowCount->x(), ui->LabelRowCount->y() - verticalOffset, ui->LabelRowCount->width(), ui->LabelRowCount->height());
    ui->ValueRowCount->setGeometry(ui->ValueRowCount->x(), ui->ValueRowCount->y() - verticalOffset, ui->ValueRowCount->width(), ui->ValueRowCount->height());

    this->setFixedSize(this->width(), this->height() - verticalOffset);
    }

    receiveModel->removeRow(receiveModel->rowCount() - 1);
    ui->ValueRowCount->setText(QString::number(receiveModel->rowCount()));
}

void ConsignmentWindow::on_addRowButton_clicked()
{
    addRow();
}

void ConsignmentWindow::on_RemoveRowButton_clicked()
{
    if (receiveModel->rowCount() == 1)
    {
        QMessageBox::warning(this, "Remove Row Error", "The minimum number of row is one.");
        return;
    }
    else
    {
        int totalRow = receiveModel->rowCount();
        int totalCol = receiveModel->columnCount();
        int colFill = 0;

        for (int col = 0; col <= totalCol - 1; col++)
        {
            //total row will return the last row to us
            itemIndex = receiveModel->index(totalRow - 1, col);
            if (itemIndex.data(0).toString() != "")
            {
                colFill += 1;
            }
        }

        if (colFill >= 1)
        {
            QString warningmsg = QString("Row %1 contains data. Are you sure you want to delete row?").arg(totalRow);
            int ans = QMessageBox::warning(this, "Data Detected", warningmsg, QMessageBox::Yes|QMessageBox::No);
            if (ans == QMessageBox::Yes)
            {
                removeRow();
            }
        }
        else
        {
            removeRow();
        }

    }

}

void ConsignmentWindow::on_CancelButton_clicked()
{
    this->reject();
}

void ConsignmentWindow::on_OkButton_clicked()
{
    if (ui->tabWidget->currentIndex() == 0)
    {
       receiveConsignment();
    }
    else if (ui->tabWidget->currentIndex() == 1)
    {
        //get the number of rows selected to use in loop
        int selectedRowCount = tableUpdate->selectionModel()->selectedRows().count();
        const QModelIndexList selectedList = tableUpdate->selectionModel()->selectedRows();

        //initialize array with desired number of columns
        QVariant tableData[5];
        int row = 0;

        //create dialog window object and pass in the row selected to add the number of rows in the dialog
        EditWindow = new ConsignmentEditWindow(selectedRowCount);
        for (QModelIndex index : selectedList)
        {
            for (int col = 0; col <= 4; col++)
            {
                tableData[col] = index.sibling(index.row(), index.column() + col).data(0).toString();
                EditWindow->setTableData(row, col, tableData[col]);
            }
            row++;
        }

        EditWindow->setModal(true);

       if (EditWindow->exec())
       {
            qDebug()<<"Payment update function will run";
       }

    }
    else if (ui->tabWidget->currentIndex() == 2)
    {
        qDebug()<<"Return function will run";
    }
}

void ConsignmentWindow::receiveConsignment()
{
    int totalRow = receiveModel->rowCount();
    int totalCol = receiveModel->columnCount();
    int validRows = 0;

    QVector<bool> rowBeginFill;
    rowBeginFill.resize(totalRow);

    QVector<int> rowColFilled;
    rowColFilled.resize(totalRow);

    for (int row = 0; row <= totalRow - 1; row++)
    {
        for (int col = 0; col <= totalCol - 1; col++)
        {
            itemIndex = receiveModel->index(row, col);
            if (itemIndex.data(0).toString() != "")
            {
                rowBeginFill[row] = true;
                rowColFilled[row] += 1;
            }
        }
    }

    for (int row = 0; row <= totalRow - 1; row++)
    {
        if (rowBeginFill[row] == true && rowColFilled[row] < 7)
        {
            QString warningmsg = QString("Row %1 is incomplete. Please fill in all the details in row %1").arg(row + 1);
            QMessageBox::warning(this, "Input Error", warningmsg);

            return;
        }
        else if (rowBeginFill[row] == true && rowColFilled[row] == 7)
        {
            validRows += 1; //count the number of valid rows first
        }
    }

    if (validRows == totalRow) //if the number of valid rows are the same as the total number of roll then go ahead
    {
        executeReceiveConsignment();
    }
    else
    {
        QMessageBox::information(this, "Input Error", "Please do not leave any rows empty.\n\nRemove the empty rows first before clicking Ok.");
        return;
    }
}

void ConsignmentWindow::executeReceiveConsignment()
{
    int product_id;
    int consignmentinvoice_id;
    int consignmenttxn_id;

    //TODO add consignment invoice
    d_db->addConsignmentInvoice(ui->ValueDate->text());

    QSqlQuery d_qry;
    QString sqlStatement = "SELECT consignmentinvoice_id FROM consignmentInvoiceTable WHERE (SELECT MAX(consignmentinvoice_id) FROM consignmentInvoiceTable)";

    d_qry.prepare(sqlStatement);

    if (d_qry.exec())
    {
        qDebug()<<"Latest Consignment Invoice query success";
    }
    else
    {
        qDebug()<<d_qry.lastError();
    }

    while(d_qry.next())
    {
        consignmentinvoice_id = d_qry.value(0).toInt();
    }

    for (int row = 0; row <= receiveModel->rowCount() - 1; row++)
    {
        QString brand = receiveModel->index(row, 1).data(0).toString();
        QString model_name = receiveModel->index(row, 2).data(0).toString();
        QString model_no = receiveModel->index(row,3).data(0).toString();
        QString serial_no = receiveModel->index(row,4).data(0).toString();
        QString cert_details = receiveModel->index(row,5).data(0).toString();

        d_db->addNewProduct(brand, model_name, model_no, serial_no, cert_details);
        //Brand Name, Model Name, Model No, Serial No, Certificate Details

        sqlStatement = "SELECT product_id FROM productTable WHERE (SELECT MAX(product_id) FROM productTable)";

        d_qry.prepare(sqlStatement);
        if (d_qry.exec())
        {
            qDebug()<<"product_id query success!";
        }
        else
        {
            qDebug()<<d_qry.lastError();
        }

        while (d_qry.next())
        {
            product_id = d_qry.value(0).toInt();
        }

        QString hrc_id = receiveModel->index(row,0).data(0).toString();
        QString hrc_cost = receiveModel->index(row,6).data(0).toString();
        QString date_consigned = ui->ValueDate->text();

        //add new line in the transaction table
        d_db->addConsignmentTxn(hrc_id, product_id, hrc_cost, date_consigned);

        //add invoice id into bridging table
        d_db->addConsignmentTxnInvoice(consignmentinvoice_id);

          sqlStatement = "SELECT consignmenttxn_id FROM consignmentTxnTable WHERE (SELECT MAX(consignmenttxn_id) FROM consignmentTxnTable)";

          d_qry.prepare(sqlStatement);
          if (d_qry.exec())
          {
              qDebug()<<"Consignment Transaction query success!";
          }
          else
          {
              qDebug()<<d_qry.lastError();
          }

          while (d_qry.next())
          {
              consignmenttxn_id = d_qry.value(0).toInt();
          }

        //add transaction id into overview table
        d_db->addConsignmentOverview(consignmenttxn_id);
    }
}

void ConsignmentWindow::updateConsignment()
{

}

void ConsignmentWindow::on_tabWidget_currentChanged(int index)
{
    QPushButton* cancelButton = ui->CancelButton;
    QPushButton* okButton = ui->OkButton;

    if (index == 0)
    {
        this->setFixedHeight(240);
        ui->tabWidget->setFixedHeight(180);
        tableReceive->setFixedHeight(71);
        tableUpdate->setFixedHeight(0);

        cancelButton->setGeometry(cancelButton->x(), 200, cancelButton->width(), cancelButton->height());
        okButton->setGeometry(okButton->x(), 200, okButton->width(), okButton->height());
    }
    else if (index == 1)
    {
        this->setFixedHeight(302);
        ui->tabWidget->setFixedHeight(242);
        tableReceive->setFixedHeight(0);
        tableUpdate->setFixedHeight(133);
        ui->LabelConsignmentPendingPayment->setGeometry(ui->LabelConsignmentPendingPayment->x(), 201, ui->LabelConsignmentPendingPayment->width(), ui->LabelConsignmentPendingPayment->height());
        ui->ValueConsignmentPendingPayment->setGeometry(ui->ValueConsignmentPendingPayment->x(), 201, ui->ValueConsignmentPendingPayment->width(), ui->ValueConsignmentPendingPayment->height());

        cancelButton->setGeometry(cancelButton->x(), 262, cancelButton->width(), cancelButton->height());
        okButton->setGeometry(okButton->x(), 262, okButton->width(), okButton->height());
    }
}
