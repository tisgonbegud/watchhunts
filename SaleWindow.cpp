#include "SaleWindow.h"
#include "ui_SaleWindow.h"


SaleWindow::SaleWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SaleWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    this->setFixedSize(QSize(845, 360));

    //install event filter to disable close by Esc Key (remember to include header QKeyEvent header file)
    this->installEventFilter(this);

    //set up the company registration no
    ui->RegistrationValue->setText("201920057M");

    //set up regular expression validator for the price input
    //QRegExpValidator rx("-?\\d");

    //set date to be today's date
    ui->ValueDate->setDate(QDate::currentDate());

    //set properties for TextAddress
    ui->TextAddress->setTabChangesFocus(true);

    //initialize ComboBox and insert list of options
    QStringList paymentmodelist = (QStringList()<<"Cash"<<"Bank Transfer"<<"Credit Card"<<"Cheque");
    QComboBox *paymentmodepicker = ui->PaymentModePicker;
    paymentmodepicker->addItems(paymentmodelist);

    //create database object to access data
    //first get the working directory of the executable then assign the database to a directory in database folder
    QString currentWd = QDir::currentPath();
    QString databaseWd = currentWd + "/database/db.sqlite";
    d_db = new dbManager(databaseWd);

    QSqlQuery d_qry;
    QString sqlStatement = "SELECT saleinvoice_id FROM saleInvoiceTable WHERE (SELECT MAX(saleinvoice_id) from saleInvoiceTable)";

    d_qry.prepare(sqlStatement);
    if (d_qry.exec())
    {
       qDebug()<<"Latest sale invoice shown";
    }
    else
    {
        qDebug()<<d_qry.lastError();
    }

    while (d_qry.next())
    {
       ui->ValueLastInvoice->setText("S-" + QString("%1").arg(d_qry.value(0).toInt(), 3, 10, QChar('0')));
    }
}

SaleWindow::~SaleWindow()
{
    delete ui;
}

void SaleWindow::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
    {
        e->accept(); //just accept the command and not escape from the dialog
    }
}

void SaleWindow::on_PaymentModePicker_currentTextChanged(const QString &arg1)
{
    if (arg1 == "Cheque")
    {
        ui->PaymentModeDetailsGroup->resize(300, ui->PaymentModeDetailsGroup->height());
        ui->LabelPaymentModeNo->show();
        ui->LabelPaymentModeNo->setText("Cheque No :");
        ui->LinePaymentModeNo->show();

    }
    else if (arg1 == "Credit Card")
    {
        ui->PaymentModeDetailsGroup->resize(300, ui->PaymentModeDetailsGroup->height());
        ui->LabelPaymentModeNo->show();
        ui->LabelPaymentModeNo->setText("Last 3/4 digits :");
        ui->LinePaymentModeNo->show();
    }
    else
    {
        ui->PaymentModeDetailsGroup->resize(160, ui->PaymentModeDetailsGroup->height());
        ui->LabelPaymentModeNo->hide();
        ui->LinePaymentModeNo->hide();
    }
}

void SaleWindow::on_OkButton_clicked()
{
    QString msgboxtitle = "Incomplete Customer Details";

    //set the default style sheet for generic message box
    QString msgboxstylesheet = "QMessageBox{"
                               "background-color: rgb(255, 255,255);"
                               "padding-top: 2px;"
                               "}"
                               "QPixmap{"
                               "padding-left: 2px;"
                               "}"
                               ""
                               "QPushButton{"
                               "border-style: solid;"
                               "border-width: 1px;"
                               "border-radius: 10px;"
                               "border-color: rgb(169, 169, 169);"
                               "background-color: rgb(255, 255, 255);"
                               "min-width: 5em;"
                               "min-height: 1em;"
                               "padding: 6px;"
                               "} "

                               "QPushButton:hover{"
                               "border-style: solid;"
                               "border-width: 1px;"
                               "border-radius: 10px;"
                               "border-color: rgb(134, 191, 160);"
                               "background-color: rgb(255, 239, 133);" //211, 240, 224 default
                               "min-width: 5em;"
                               "min-height: 1em;"
                               "padding: 6px;"
                               "}";

    if (ui->LineName->text() == "")
    {
        QString msgtext = "Please enter the customer's name.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->LineName->setFocus();
        return;
    }
    else if (ui->LineContactNo->text() == "")
    {
        QString msgtext = "Please enter the customer's contact number.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->LineContactNo->setFocus();
        return;
    }
    else if(ui->LineNRIC->text() == "")
    {
        QString msgtext = "Please enter the customer's NRIC/Passport number.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->LineNRIC->setFocus();
        return;
    }
    else if(ui->TextAddress->toPlainText()== "")
    {
        QString msgtext = "Please enter the customer's address.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->TextAddress->setFocus();
        return;
    }
    else if (ui->PaymentModePicker->currentText() == "Cheque" && ui->LinePaymentModeNo->text() == "")
    {
        QString msgtext = "Please enter cheque number.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->LinePaymentModeNo->setFocus();
        return;
    }
    else if (ui->PaymentModePicker->currentText() == "Credit Card" && ui->LinePaymentModeNo->text() == "")
    {
        QString msgtext = "Please enter last 3 or 4 digits of credit card.\n\nYou may key in more than one number.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->LinePaymentModeNo->setFocus();
        return;
    }

    bool rowBeginFill[5] = {0};
    int rowFilledCount[5] = {false};

    auto row1 = {ui->LineID1, ui->LinePrice1, ui->LineRemarks1};
    auto row2 = {ui->LineID2, ui->LinePrice2, ui->LineRemarks2};
    auto row3 = {ui->LineID3, ui->LinePrice3, ui->LineRemarks3};
    auto row4 = {ui->LineID4, ui->LinePrice4, ui->LineRemarks4};
    auto row5 = {ui->LineID5, ui->LinePrice5, ui->LineRemarks5};

    for (auto field : row1)
    {
        if (field->text() != "")
        {
            rowBeginFill[0] = true;
            rowFilledCount[0] += 1;
        }
    }

    for (auto field : row2)
    {
        if (field->text() != "")
        {
            rowBeginFill[1] = true;
            rowFilledCount[1] += 1;
        }
    }

    for (auto field : row3)
    {
        if (field->text() != "")
        {
            rowBeginFill[2] = true;
            rowFilledCount[2] += 1;
        }
    }
    for (auto field : row4)
    {
        if (field->text() != "")
        {
            rowBeginFill[3] = true;
            rowFilledCount[3] += 1;
        }
    }
    for (auto field : row5)
    {
        if (field->text() != "")
        {
            rowBeginFill[4] = true;
            rowFilledCount[4] += 1;
        }
    }

    int filledRowCount = 0;

    for (int i = 0; i <= 4; i++)
    {

        if (rowBeginFill[i] == true && rowFilledCount[i] == 3)
        {
            filledRowCount += 1;
        }
        else if (rowBeginFill[i] == true && rowFilledCount[i] < 3)
        {
            QString warningmsg = QStringLiteral("Row %1 is incomplete. Please check all the fields in row %1.").arg(i + 1);
            QMessageBox warningbox;
            warningbox.setText(warningmsg);
            warningbox.setWindowTitle("Input Error");
            warningbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
            warningbox.setStyleSheet(msgboxstylesheet);

            warningbox.exec();

            return;
         }
    }

    if (filledRowCount == 0)
    {
        QString warningmsg = "Please enter at least one product.";
        QMessageBox warningbox;
        warningbox.setText(warningmsg);
        warningbox.setWindowTitle("Input Error");
        warningbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        warningbox.setStyleSheet(msgboxstylesheet);

        warningbox.exec();

        return;
    }

    //if all the customer's details are enabled for editing, it means it is a new customer
    if (ui->LineName->isEnabled() && ui->LineContactNo->isEnabled() &&
            ui->LineNRIC->isEnabled() && ui->TextAddress->isEnabled())
    {
        d_db->addNewCustomer(ui->LineName->text(), ui->LineContactNo->text(),
                             ui->LineNRIC->text(), ui->TextAddress->toPlainText());
    }

    executeRecordTransaction();
    QMessageBox::information(this, "Operation Success", "Sales was successfully recorded.");
    this->accept(); //TODO move this into a bool function and only run when it returns true
}

void SaleWindow::executeRecordTransaction()
{
    QLineEdit *stockIdIndex[5] = {ui->LineID1, ui->LineID2, ui->LineID3, ui->LineID4, ui->LineID5};
    QLineEdit *sellingPriceIndex[5] = {ui->LinePrice1, ui->LinePrice2, ui->LinePrice3, ui->LinePrice4, ui->LinePrice5};
    QLineEdit *remarksIndex[5] {ui->LineRemarks1, ui->LineRemarks2, ui->LineRemarks3, ui->LineRemarks4, ui->LineRemarks5};

    int product_id;
    int customer_id;
    int saletxn_id;
    int saleinvoice_id;

    //execute sql query first to select the customer_id
    QString sqlStatement = "SELECT customer_id FROM customersTable WHERE name = :name";

    QSqlQuery d_qry;
    d_qry.prepare(sqlStatement);
    d_qry.bindValue(":name", ui->LineName->text());

    if (d_qry.exec())
    {
        qDebug()<<"customer_id query success!";
    }
    else
    {
        qDebug()<<"customer id error :"<<d_qry.lastError();
    }
    while(d_qry.next())
    {
        customer_id = d_qry.value(0).toInt();
    }

    //create a new purchase invoice (before the loop because we only need one invoice number
    d_db->addSaleInvoice(ui->ValueDate->text());

    //iterate through the first column to determine which row has data,
    //if the cell is filled, the rest of the data in the same row will be added into product table

    QString WHstring = "WH";
    QString CString = "C";

    for (int i = 0; i <= 4; i++)
    {
        if(stockIdIndex[i]->text().indexOf(WHstring, 0, Qt::CaseInsensitive) != -1)
            //logically equivalent to not absent (or is present)
        {
            qDebug()<<"WH stock found";
            int stock_id = stockIdIndex[i]->text().mid(
                        stockIdIndex[i]->text().indexOf(WHstring, 0, Qt::CaseInsensitive) + 2
                        ).toInt();

            //sql query to find the product id
            sqlStatement = "SELECT product_id FROM stockOverviewTable"
                           "    JOIN purchaseTxnTable"
                           "        USING (purchasetxn_id)"
                           "WHERE stock_id = :stock_id";
            d_qry.prepare(sqlStatement);
            d_qry.bindValue(":stock_id", stock_id);

            if (d_qry.exec())
            {
                qDebug()<<"product_id retrieved";
            }
            else
            {
                qDebug()<<d_qry.lastError();
            }

            while (d_qry.next())
            {
                product_id = d_qry.value(0).toInt();
            }

            //add salestxn into table, passing in selling price, date sold, customer_id and product_id (4 arguments)
                if (ui->PaymentModePicker->currentText() == "Cheque")
                {
                    d_db->addSaleTxn(sellingPriceIndex[i]->text().toFloat(), ui->ValueDate->text(), ui->PaymentModePicker->currentText(),
                                     ui->LinePaymentModeNo->text(), "NA",
                                     remarksIndex[i]->text(), customer_id, product_id);
                }
                else if (ui->PaymentModePicker->currentText() == "Credit Card")
                {
                    d_db->addSaleTxn(sellingPriceIndex[i]->text().toFloat(), ui->ValueDate->text(), ui->PaymentModePicker->currentText(),
                                     "NA", ui->LinePaymentModeNo->text(),
                                     remarksIndex[i]->text(), customer_id, product_id);
                }
                else
                {
                    d_db->addSaleTxn(sellingPriceIndex[i]->text().toFloat(), ui->ValueDate->text(), ui->PaymentModePicker->currentText(),
                                     "NA", "NA",
                                     remarksIndex[i]->text(), customer_id, product_id);
                }

            //sql query to get latest added salestxn id
                sqlStatement = "SELECT saletxn_id FROM saleTxnTable WHERE (SELECT MAX(saletxn_id) from saleTxnTable)";

                d_qry.prepare(sqlStatement);
                if (d_qry.exec())
                {
                    qDebug()<<"saletxn_id query success!";
                }
                else
                {
                    qDebug()<<d_qry.lastError();
                }

                while (d_qry.next())
                {
                   saletxn_id = d_qry.value(0).toInt();
                }
                //add salestxn into the stockoverview table
                    d_db->addSaleStockOverview(saletxn_id, stock_id);

                //do another sql query to find the latest added sale invoice to insert into the bridging table
                sqlStatement = "SELECT saleinvoice_id FROM saleInvoiceTable WHERE (SELECT MAX(saleinvoice_id) from saleInvoiceTable)";
                d_qry.prepare(sqlStatement);
                if (d_qry.exec())
                {
                    qDebug()<<"saleinvoice_id query success!";
                }
                else
                {
                    qDebug()<<d_qry.lastError();
                }

                while (d_qry.next())
                {
                   saleinvoice_id = d_qry.value(0).toInt();
                }

                //add salestxn into salestxninvoicetable
                d_db->addSaleTxnInvoice(saleinvoice_id);
        }
        //TODO add else if for consignment
        else if (stockIdIndex[i]->text().indexOf(CString, 0, Qt::CaseInsensitive) != -1)
        {
             qDebug()<<"C stock found";
             int consignment_id = stockIdIndex[i]->text().mid(
                         stockIdIndex[i]->text().indexOf(CString, 0, Qt::CaseInsensitive) + 1
                         ).toInt();

             //sql query to find the product id
             sqlStatement = "SELECT product_id FROM consignmentOverviewTable"
                            "    JOIN consignmentTxnTable"
                            "        USING (consignmenttxn_id)"
                            "WHERE consignment_id = :consignment_id";
             d_qry.prepare(sqlStatement);
             d_qry.bindValue(":consignment_id", consignment_id);

             if (d_qry.exec())
             {
                 qDebug()<<"product_id retrieved";
             }
             else
             {
                 qDebug()<<d_qry.lastError();
             }

             while (d_qry.next())
             {
                 product_id = d_qry.value(0).toInt();
             }

             if (ui->PaymentModePicker->currentText() == "Cheque")
             {
                 d_db->addSaleTxn(sellingPriceIndex[i]->text().toFloat(), ui->ValueDate->text(), ui->PaymentModePicker->currentText(),
                                  ui->LinePaymentModeNo->text(), "NA",
                                  remarksIndex[i]->text(), customer_id, product_id);
             }
             else if (ui->PaymentModePicker->currentText() == "Credit Card")
             {
                 d_db->addSaleTxn(sellingPriceIndex[i]->text().toFloat(), ui->ValueDate->text(), ui->PaymentModePicker->currentText(),
                                  "NA", ui->LinePaymentModeNo->text(),
                                  remarksIndex[i]->text(), customer_id, product_id);
             }
             else
             {
                 d_db->addSaleTxn(sellingPriceIndex[i]->text().toFloat(), ui->ValueDate->text(), ui->PaymentModePicker->currentText(),
                                  "NA", "NA",
                                  remarksIndex[i]->text(), customer_id, product_id);
             }

             //sql query to get latest added salestxn id
                 sqlStatement = "SELECT saletxn_id FROM saleTxnTable WHERE (SELECT MAX(saletxn_id) from saleTxnTable)";

                 d_qry.prepare(sqlStatement);
                 if (d_qry.exec())
                 {
                     qDebug()<<"saletxn_id query success!";
                 }
                 else
                 {
                     qDebug()<<d_qry.lastError();
                 }

                 while (d_qry.next())
                 {
                    saletxn_id = d_qry.value(0).toInt();
                 }
                 //add salestxn into the stockoverview table
                     d_db->addSaleConsignmentOverview(saletxn_id, consignment_id);

                 //do another sql query to find the latest added sale invoice to insert into the bridging table
                 sqlStatement = "SELECT saleinvoice_id FROM saleInvoiceTable WHERE (SELECT MAX(saleinvoice_id) from saleInvoiceTable)";
                 d_qry.prepare(sqlStatement);
                 if (d_qry.exec())
                 {
                     qDebug()<<"saleinvoice_id query success!";
                 }
                 else
                 {
                     qDebug()<<d_qry.lastError();
                 }

                 while (d_qry.next())
                 {
                    saleinvoice_id = d_qry.value(0).toInt();
                 }

                 //add salestxn into salestxninvoicetable
                 d_db->addSaleTxnInvoice(saleinvoice_id);
        }
    }
}

void SaleWindow::on_CustomerClearButton_clicked()
{
    if (ui->LineName->text() != "" || ui->LineContactNo->text() != "" || ui->LineNRIC->text() != ""
            || ui->TextAddress->toPlainText() != "")
    {
    int answer = QMessageBox::warning(this, "Clear Customer Details", "Customer data will be cleared and you will lose unsaved changes.\n\nAre you sure?",
                         QMessageBox::No | QMessageBox::Yes);
        if (answer == QMessageBox::Yes)
            {
                //clear the customer details when user click on clear
                ui->LineName->clear();
                ui->LineContactNo->clear();
                ui->LineNRIC->clear();
                ui->TextAddress->clear();

                //set focus on the first first box after clearing for ease of typing
                ui->LineName->setFocus();

                //disable the edit button if it's turn on. Otherwise no net effect
                ui->EditButton->setEnabled(false);
            }
    }
}

void SaleWindow::on_AddNewCustomerButton_clicked()
{
    if (ui->LineName->isEnabled() == false &&
            ui->LineContactNo->isEnabled() == false &&
            ui->LineNRIC->isEnabled() == false &&
            ui->TextAddress->isEnabled() == false)
    {
        if (ui->LineName->text() == "" &&
                ui->LineContactNo->text() == "" &&
                ui->LineNRIC->text() == "" &&
                ui->TextAddress->toPlainText() == "")
        {
            //disable the add customer button so users cannot click again
            ui->AddNewCustomerButton->setEnabled(false);

            //enable the textbox when user click on add customer button
            ui->LineName->setDisabled(false);
            ui->LineContactNo->setDisabled(false);
            ui->LineNRIC->setDisabled(false);
            ui->TextAddress->setDisabled(false);

            //set focus on the first box for ease of typing
            ui->LineName->setFocus();
        }
        else
        {
            int ans = QMessageBox::warning(this, "Customer Details Found", "Customer details currently in the form will be cleared.\n\nAre you sure?", QMessageBox::No|QMessageBox::Yes);
                if (ans == QMessageBox::Yes)
                {
                    //disable the add customer button so users cannot click again
                    ui->AddNewCustomerButton->setEnabled(false);

                    //clear all data in the textbox first
                    ui->LineName->clear();
                    ui->LineContactNo->clear();
                    ui->LineNRIC->clear();
                    ui->TextAddress->clear();

                    //enable the textbox when user click on add customer button
                    ui->LineName->setDisabled(false);
                    ui->LineContactNo->setDisabled(false);
                    ui->LineNRIC->setDisabled(false);
                    ui->TextAddress->setDisabled(false);

                    //set focus on the first box for ease of typing
                    ui->LineName->setFocus();
                }
        }
    }
}

void SaleWindow::on_CustomerSearchButton_clicked()
{
    if (ui->LineName->text() != "" || ui->LineContactNo->text() != "" || ui->LineNRIC->text() != ""
            || ui->TextAddress->toPlainText() != "")
    {
    int answer = QMessageBox::warning(this, "Clear Customer Details", "You have entered some customer's details.\nAll unsaved data will be lost.\n\nAre you sure?",
                         QMessageBox::No | QMessageBox::Yes);
        if (answer == QMessageBox::Yes)
            {
                //clear the customer details when user click on clear
                ui->LineName->clear();
                ui->LineContactNo->clear();
                ui->LineNRIC->clear();
                ui->TextAddress->clear();

                //disable the text field again after clearing
                ui->LineName->setEnabled(false);
                ui->LineContactNo->setEnabled(false);
                ui->LineNRIC->setEnabled(false);
                ui->TextAddress->setEnabled(false);

                //enable add new customer button if user wants to search
                ui->AddNewCustomerButton->setEnabled(true);

                customerSearchWin = new CustomerSearch();
                customerSearchWin->setModal(true);

                //if customerSearchWin exec returns 1, get data from the customer search window
                if (customerSearchWin->exec())
                {
                    ui->LineName->setText(customerSearchWin->getName());
                    ui->LineContactNo->setText(customerSearchWin->getContactNo());
                    ui->LineNRIC->setText(customerSearchWin->getNricNo());
                    ui->TextAddress->setText(customerSearchWin->getAddress());
                    ui->EditButton->setEnabled(true);
                }
                //else do nothing and just print a message
                else if (customerSearchWin->result() == QDialog::Rejected)
                {
                    qDebug()<<"Customer search operation cancelled by user";
                }
            }
     }
    else
    {
        //disable the text field again if the fields are already empty to prevent input after searching
        ui->LineName->setEnabled(false);
        ui->LineContactNo->setEnabled(false);
        ui->LineNRIC->setEnabled(false);
        ui->TextAddress->setEnabled(false);

        //enable add new customer button if user wants to search
        ui->AddNewCustomerButton->setEnabled(true);

        customerSearchWin = new CustomerSearch();
        customerSearchWin->setModal(true);

        //if customerSearchWin exec returns 1, get data from the customer search window
        if (customerSearchWin->exec())
        {
            ui->LineName->setText(customerSearchWin->getName());
            ui->LineContactNo->setText(customerSearchWin->getContactNo());
            ui->LineNRIC->setText(customerSearchWin->getNricNo());
            ui->TextAddress->setText(customerSearchWin->getAddress());
            ui->EditButton->setEnabled(true);
        }
        //else do nothing and just print a message
        else if (customerSearchWin->result() == QDialog::Rejected)
        {
            qDebug()<<"Customer search operation cancelled by user";
        }
    }
}

void SaleWindow::on_LineName_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        ui->CustomerClearButton->setEnabled(true);
    }
    else if(arg1 == "" && ui->LineContactNo->text() == "" && ui->LineNRIC->text() == "" && ui->TextAddress->toPlainText() == "")
    {
        ui->CustomerClearButton->setEnabled(false);
    }
}

void SaleWindow::on_LineContactNo_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        ui->CustomerClearButton->setEnabled(true);
    }
    else if(arg1 == "" && ui->LineName->text() == "" && ui->LineNRIC->text() == "" && ui->TextAddress->toPlainText() == "")
    {
        ui->CustomerClearButton->setEnabled(false);
    }
}

void SaleWindow::on_LineNRIC_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        ui->CustomerClearButton->setEnabled(true);
    }
    else if(arg1 == "" && ui->LineContactNo->text() == "" && ui->LineContactNo->text() == "" && ui->TextAddress->toPlainText() == "")
    {
        ui->CustomerClearButton->setEnabled(false);
    }
}

void SaleWindow::on_TextAddress_textChanged()
{
    if (ui->TextAddress->toPlainText() != "")
    {
        ui->CustomerClearButton->setEnabled(true);
    }
    else if(ui->LineName->text() == "" && ui->LineContactNo->text() == "" && ui->LineNRIC->text() == "" && ui->TextAddress->toPlainText() == "")
    {
        ui->CustomerClearButton->setEnabled(false);
    }
}

void SaleWindow::on_BackButton_clicked()
{
    bool bcustomerFilled = false;
    bool bproductFilled = false;

    if (ui->LineName->text() != "" ||
            ui->LineContactNo->text() != "" ||
            ui->LineNRIC->text() != "" ||
            ui->TextAddress->toPlainText() != ""
            )
    {
    bcustomerFilled = true;
    }

    if(ui->LineID1->text() != "" ||
            ui->LineID2->text() != "" ||
            ui->LineID3->text() != "" ||
            ui->LineID4->text() != "" ||
            ui->LineID5->text() != "" ||
            ui->LinePrice1->text() != "" ||
            ui->LinePrice2->text() != "" ||
            ui->LinePrice3->text() != "" ||
            ui->LinePrice4->text() != "" ||
            ui->LinePrice5->text() != "" ||
            ui->LineRemarks1->text() != "" ||
            ui->LineRemarks2->text() != "" ||
            ui->LineRemarks3->text() != "" ||
            ui->LineRemarks4->text() != "" ||
            ui->LineRemarks5->text() != "" )
    {
        bproductFilled = true;
    }

    if (bcustomerFilled && !bproductFilled)
    {
        int ans = QMessageBox::warning(this, "Customer Details Found", "You have entered some customer data.\n\nAre you sure you want to go back?\n\n(You will lose unsaved changes)", QMessageBox::No|QMessageBox::Yes);
             if (ans == QMessageBox::Yes)
             {
                 this->close();
             }
             else
             {
                 return;
             }
    }
    else if (bproductFilled && !bcustomerFilled)
    {
        int ans = QMessageBox::warning(this, "Product Details Found", "You have entered some product data.\n\nAre you sure you want to go back?\n\n(You will lose unsaved changes)", QMessageBox::No|QMessageBox::Yes);
             if (ans == QMessageBox::Yes)
             {
                 this->close();
             }
             else
             {
                 return;
             }
    }
    else if(bproductFilled && bcustomerFilled)
    {
        int ans = QMessageBox::warning(this, "Customer And Product Details Found", "You have entered some customer and product data.\n\nAre you sure you want to go back?\n\n(You will lose unsaved changes)", QMessageBox::No|QMessageBox::Yes);
             if (ans == QMessageBox::Yes)
             {
                 this->close();
             }
             else
             {
                 return;
             }
    }
    else
    {
        //close the dialog window if the every detail is blank
        this->close();
    }
}

void SaleWindow::on_ProductSearchButton_clicked()
{
    QLineEdit* LineID[5] = {ui->LineID1, ui->LineID2, ui->LineID3, ui->LineID4, ui->LineID5};
    productSearchWin = new ProductSearch();
    productSearchWin->setModal(true);

    auto productCollection = {ui->LineID1, ui->LineID2, ui->LineID3, ui->LineID4, ui->LineID5,
                             ui->LinePrice1, ui->LinePrice2, ui->LinePrice3, ui->LinePrice4, ui->LinePrice5,
                             ui->LineRemarks1, ui->LineRemarks2, ui->LineRemarks3, ui->LineRemarks4, ui->LineRemarks5};

   int productFilledCount = 0;

    for (auto field: productCollection)
    {
        if (field->text() != "")
        {
            productFilledCount += 1;
        }

    }

    if (productFilledCount > 0)
    {
        int answer = QMessageBox::warning(this, "Product Details Found", "Product data will be cleared and you will lose unsaved changes.\n\nAre you sure?", QMessageBox::No|QMessageBox::Yes);
            if (answer == QMessageBox::Yes)
            {
                for (auto field : productCollection)
                {
                    field->clear();
                    ui->ProductClearButton->setEnabled(false);
                }

                if (productSearchWin->exec())
                {
                    for (int i = 0; i <= productSearchWin->getSelectedCount() - 1;i++)
                    {
                        LineID[i]->setText(productSearchWin->getStockId(i));
                    }
                    ui->ProductClearButton->setEnabled(true);
                }

                else if (productSearchWin->result() == QDialog::Rejected)
                {
                    qDebug()<<"Product search cancelled by user";
                }
            }

            return;
    }
    else
    {
        if (productSearchWin->exec())
        {
            for (int i = 0; i <= productSearchWin->getSelectedCount() - 1;i++)
            {
                LineID[i]->setText(productSearchWin->getStockId(i));
            }
            ui->ProductClearButton->setEnabled(true);
        }

        else if (productSearchWin->result() == QDialog::Rejected)
        {
            qDebug()<<"Product search cancelled by user";
        }
    }
}

void SaleWindow::on_ProductClearButton_clicked()
{
    auto productCollection = {ui->LineID1, ui->LineID2, ui->LineID3, ui->LineID4, ui->LineID5,
                             ui->LinePrice1, ui->LinePrice2, ui->LinePrice3, ui->LinePrice4, ui->LinePrice5,
                             ui->LineRemarks1, ui->LineRemarks2, ui->LineRemarks3, ui->LineRemarks4, ui->LineRemarks5};

   int productFilledCount = 0;

    for (auto field: productCollection)
    {
        if (field->text() != "")
        {
            productFilledCount += 1;
        }

    }

    if (productFilledCount > 0)
    {
        int answer = QMessageBox::warning(this, "Product Details Found", "Product data will be cleared and you will lose unsaved changes.\n\nAre you sure?", QMessageBox::No|QMessageBox::Yes);
            if (answer == QMessageBox::Yes)
            {
                for (auto field : productCollection)
                {
                    field->clear();
                    ui->ProductClearButton->setEnabled(false);
                }

                //Set the focus on the first line in the product details after clearing
                ui->LinePrice1->setFocus();
            }
    }
}

void SaleWindow::productClearButton_enabler(const QString &arg1)
{
    auto productCollection = {ui->LineID1, ui->LineID2, ui->LineID3, ui->LineID4, ui->LineID5,
                             ui->LinePrice1, ui->LinePrice2, ui->LinePrice3, ui->LinePrice4, ui->LinePrice5,
                             ui->LineRemarks1, ui->LineRemarks2, ui->LineRemarks3, ui->LineRemarks4, ui->LineRemarks5};

    for (auto field : productCollection)
    {
        field->text() = arg1;

        if (arg1 != "")
        {
            //just enable the clear button once
            ui->ProductClearButton->setEnabled(true);
            return;
        }
        else
        {
            ui->ProductClearButton->setEnabled(false);
        }
    }
}

void SaleWindow::on_LinePrice1_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);

}

void SaleWindow::on_LinePrice2_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void SaleWindow::on_LinePrice3_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void SaleWindow::on_LinePrice4_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void SaleWindow::on_LinePrice5_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void SaleWindow::on_LineRemarks1_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void SaleWindow::on_LineRemarks2_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void SaleWindow::on_LineRemarks3_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void SaleWindow::on_LineRemarks4_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void SaleWindow::on_LineRemarks5_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void SaleWindow::on_EditButton_clicked()
{
    //TODO implement a function edit the customer's data
}
