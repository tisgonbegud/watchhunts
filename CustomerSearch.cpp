#include "CustomerSearch.h"
#include "ui_CustomerSearch.h"

CustomerSearch::CustomerSearch(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CustomerSearch)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::Dialog);
    this->setFixedSize(QSize(721, 220));

    //install event filter to disable close by Esc Key (remember to include header QKeyEvent header file)
    this->installEventFilter(this);

    //create database object to access data
    //first get the working directory of the executable then assign the database to a directory in database folder
    QString currentWd = QDir::currentPath();
    QString databaseWd = currentWd + "/database/db.sqlite";
    d_db = new dbManager(databaseWd);

    //Displays all customers data in the beginning
    modal = new QSqlQueryModel();
    QString sqlStatement = "SELECT name, contact_no, nric_no, address from customersTable;";
    QSqlQuery d_qry;

    d_qry.prepare(sqlStatement);
    d_qry.exec();

    modal->setQuery(d_qry);
    modal->setHeaderData(0, Qt::Horizontal, tr("Name"));
    modal->setHeaderData(1, Qt::Horizontal, tr("Contact No"));
    modal->setHeaderData(2, Qt::Horizontal, tr("NRIC/Passport No"));
    modal->setHeaderData(3, Qt::Horizontal, tr("Address"));

    tableModel = ui->TableResult;

    tableModel->setModel(modal);
    tableModel->resizeColumnsToContents();
    tableModel->resizeRowsToContents();
    tableModel->horizontalHeader()->setHighlightSections(false);
    tableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    tableModel->horizontalHeader()->setStretchLastSection(true);
    tableModel->horizontalHeader()->setMinimumSectionSize(100);

}

CustomerSearch::~CustomerSearch()
{
    delete ui;
}

void CustomerSearch::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
    {
        e->accept(); //just accept the command and not escape from the dialog
    }
}

void CustomerSearch::on_CancelButton_clicked()
{
    this->reject();
}

void CustomerSearch::on_SelectButton_clicked()
{
    if (tableModel->selectionModel()->currentIndex().row() == -1)
    {
        QMessageBox::warning(this, "Selection Error", "Please select a customer");
    }
    else
    {
      this->accept();
    }
}

QString CustomerSearch::getName()
{
    rowNum = tableModel->selectionModel()->currentIndex().row();
    itemModel = tableModel->model();
    index = itemModel->index(rowNum, 0); //Name
    QString nameData = index.data().toString();
    return nameData;
}

QString CustomerSearch::getContactNo()
{
    rowNum = tableModel->selectionModel()->currentIndex().row();
    itemModel = tableModel->model();
    index = itemModel->index(rowNum, 1); //Contact No
    QString contactNoData = index.data().toString();
    return contactNoData;
}

QString CustomerSearch::getNricNo()
{
    rowNum = tableModel->selectionModel()->currentIndex().row();
    itemModel = tableModel->model();
    index = itemModel->index(rowNum, 2); //NRIC
    QString nricNoData = index.data().toString();
    return nricNoData;
}

QString CustomerSearch::getAddress()
{
    rowNum = tableModel->selectionModel()->currentIndex().row();
    itemModel = tableModel->model();
    index = itemModel->index(rowNum, 3); //Address
    QString addressData = index.data().toString();
    return addressData;
}


void CustomerSearch::on_LineName_textChanged(const QString &arg1)
{
    //implement a search function here
    QString sqlStatement = "SELECT name, contact_no, nric_no, address FROM customersTable WHERE name LIKE '%' || :name || '%'";
    QSqlQuery d_qry;
    d_qry.prepare(sqlStatement);
    d_qry.bindValue(":name", arg1);

    if (d_qry.exec())
    {
        qDebug()<<"Attempting to find customer by name...";

        modal->setQuery(d_qry);
        modal->setHeaderData(0, Qt::Horizontal, tr("Name"));
        modal->setHeaderData(1, Qt::Horizontal, tr("Contact No"));
        modal->setHeaderData(2, Qt::Horizontal, tr("NRIC/Passport No"));
        modal->setHeaderData(3, Qt::Horizontal, tr("Address"));
    }
    else
    {
        qDebug()<<d_qry.lastError();
    }

    while (d_qry.next())
    {
        //Filter the result
        modal->setQuery(d_qry);
        modal->setHeaderData(0, Qt::Horizontal, tr("Name"));
        modal->setHeaderData(1, Qt::Horizontal, tr("Contact No"));
        modal->setHeaderData(2, Qt::Horizontal, tr("NRIC/Passport No"));
        modal->setHeaderData(3, Qt::Horizontal, tr("Address"));

        tableModel->setModel(modal);
        tableModel->resizeColumnsToContents();
        tableModel->horizontalHeader()->setHighlightSections(false);
        tableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
        tableModel->horizontalHeader()->setStretchLastSection(true);
     }
}
