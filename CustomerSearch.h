#ifndef CUSTOMERSEARCH_H
#define CUSTOMERSEARCH_H

#include <QDialog>
#include <QKeyEvent>
#include "dbManager.h"
#include <QTableView>
#include <QMessageBox>

namespace Ui {
class CustomerSearch;
}

class CustomerSearch : public QDialog
{
    Q_OBJECT

public:
    explicit CustomerSearch(QWidget *parent = nullptr);
    ~CustomerSearch();

    QString getName();
    QString getContactNo();
    QString getNricNo();
    QString getAddress();

private slots:
    void keyPressEvent(QKeyEvent *e);

    void on_CancelButton_clicked();

    void on_SelectButton_clicked();

    void on_LineName_textChanged(const QString &arg1);

private:
    Ui::CustomerSearch *ui;

    dbManager *d_db;

    QSqlQueryModel *modal;

    QTableView *tableModel;

    //used in getting values
    int rowNum;

    //used in getting values
    QAbstractItemModel *itemModel;

    //used in getting values
    QModelIndex index;

};

#endif // CUSTOMERSEARCH_H
