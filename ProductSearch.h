#ifndef PRODUCTSEARCH_H
#define PRODUCTSEARCH_H

#include <QDialog>
#include <QKeyEvent>
#include "dbManager.h"
#include <QTableView>
#include <QMessageBox>

namespace Ui {
class ProductSearch;
}

class ProductSearch : public QDialog
{
    Q_OBJECT

public:
    explicit ProductSearch(QWidget *parent = nullptr);
    ~ProductSearch();

    QString getStockId(const int& i);

    int getSelectedCount();

private slots:
    void on_CancelButton_clicked();

    void on_ConsignmentCheckBox_stateChanged(int arg1);

    void keyPressEvent(QKeyEvent *e);

    void on_LineSWHID_textChanged(const QString &arg1);

    void on_LineSBrand_textChanged(const QString &arg1);

    void on_LineSModelName_textChanged(const QString &arg1);

    void on_LineSModelNo_textChanged(const QString &arg1);

    void on_LineSSerialNo_textChanged(const QString &arg1);

    void on_TableS_clicked(const QModelIndex &index);

    void on_SelectButton_clicked();

    void on_TableC_clicked(const QModelIndex &index);

private:
    Ui::ProductSearch *ui;

    QSize closedSize;
    QSize openedSize;

    dbManager *d_db;

    QSqlQueryModel *stockModel;
    QSqlQueryModel *consignmentModel;

    QTableView *stockTableModel;
    QTableView *consignmentTableModel;

};

#endif // PRODUCTSEARCH_H
