#ifndef CONSIGNMENTEDITWINDOW_H
#define CONSIGNMENTEDITWINDOW_H

#include <QDialog>
#include <QTableView>
#include <QStandardItemModel>

namespace Ui {
class ConsignmentEditWindow;
}

class ConsignmentEditWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ConsignmentEditWindow(const int& rowSelected, QWidget *parent = nullptr);
    ~ConsignmentEditWindow();

    void setTableData(const int& row, const int& col, const QVariant& value);

private:
    Ui::ConsignmentEditWindow *ui;

    QTableView* tableModel;
    QStandardItemModel* itemModel;



};

#endif // CONSIGNMENTEDITWINDOW_H
