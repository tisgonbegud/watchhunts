#include "ProductSearch.h"
#include "ui_ProductSearch.h"

ProductSearch::ProductSearch(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProductSearch)
{
    ui->setupUi(this);

    //hide the window title bar and the close button
    this->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::Dialog);

    //default size of the window for consignment table view
    closedSize = QSize(this->width(), 330);
    openedSize = QSize(this->width(), 620);
    this->setFixedSize(closedSize);

    //disable and hide the consignment on init
    ui->GroupConsignment->setVisible(false);
    //move the buttons from the button to up on top
    int verticalOffset = 295;
    ui->CancelButton->setGeometry(ui->CancelButton->geometry().x(), ui->CancelButton->geometry().y() - verticalOffset,
                                  ui->CancelButton->geometry().width(), ui->CancelButton->geometry().height());
    ui->ConsignmentCheckBox->setGeometry(ui->ConsignmentCheckBox->geometry().x(), ui->ConsignmentCheckBox->geometry().y() - verticalOffset,
                                         ui->ConsignmentCheckBox->geometry().width(), ui->ConsignmentCheckBox->geometry().height());
    ui->SelectButton->setGeometry(ui->SelectButton->geometry().x(), ui->SelectButton->geometry().y() - verticalOffset,
                                  ui->SelectButton->geometry().width(), ui->SelectButton->geometry().height());
    ui->LabelInform1->setGeometry(ui->LabelInform1->geometry().x(), ui->LabelInform1->geometry().y() - verticalOffset,
                                  ui->LabelInform1->geometry().width(), ui->LabelInform1->geometry().height());
    ui->LabelInform2->setGeometry(ui->LabelInform2->geometry().x(), ui->LabelInform2->geometry().y() - verticalOffset,
                                  ui->LabelInform2->geometry().width(), ui->LabelInform2->geometry().height());
    ui->LabelInform3->setGeometry(ui->LabelInform3->geometry().x(), ui->LabelInform3->geometry().y() - verticalOffset,
                                  ui->LabelInform3->geometry().width(), ui->LabelInform3->geometry().height());

    //create database object to access data
    //first get the working directory of the executable then assign the database to a directory in database folder
    QString currentWd = QDir::currentPath();
    QString databaseWd = currentWd + "/database/db.sqlite";
    d_db = new dbManager(databaseWd);

    //Displays all stock data in the beginning
    stockModel = new QSqlQueryModel();
    QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                           "FROM stockOverviewTable"
                           "    JOIN purchaseTxnTable"
                           "        USING (purchasetxn_id)"
                           "    JOIN productTable"
                           "        USING (product_id)"
                           "WHERE saletxn_id IS NULL";

    QSqlQuery d_qry;

    d_qry.prepare(sqlStatement);
    d_qry.exec();

    stockModel->setQuery(d_qry);
    stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
    stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
    stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
    stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
    stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

    stockTableModel = ui->TableS;

    stockTableModel->setModel(stockModel);
    stockTableModel->resizeColumnsToContents();
    stockTableModel->horizontalHeader()->setHighlightSections(false);
    stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    stockTableModel->horizontalHeader()->setStretchLastSection(true);
    stockTableModel->horizontalHeader()->setMinimumSectionSize(70);


    //Displays all consignment data in the beginning
    consignmentModel = new QSqlQueryModel();
    sqlStatement = "SELECT 'C' || printf('%03d', consignment_id), brand_name, model_name, model_no, serial_no "
                           "FROM consignmentOverviewTable"
                           "    LEFT JOIN consignmentTxnTable"
                           "    USING (consignmenttxn_id)"
                           "    LEFT JOIN productTable"
                           "    USING (product_id)"
                           "WHERE saletxn_id IS NULL";

    d_qry.prepare(sqlStatement);
    d_qry.exec();

    consignmentModel->setQuery(d_qry);
    consignmentModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
    consignmentModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
    consignmentModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
    consignmentModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
    consignmentModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

    consignmentTableModel = ui->TableC;

    consignmentTableModel->setModel(consignmentModel);
    consignmentTableModel->resizeColumnsToContents();
    consignmentTableModel->horizontalHeader()->setHighlightSections(false);
    consignmentTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    consignmentTableModel->horizontalHeader()->setStretchLastSection(true);
    consignmentTableModel->horizontalHeader()->setMinimumSectionSize(70);
}

ProductSearch::~ProductSearch()
{
    delete ui;
}

void ProductSearch::on_CancelButton_clicked()
{
    this->reject();
}

void ProductSearch::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
    {
        e->accept(); //just accept the command and not escape from the dialog
    }
}

void ProductSearch::on_ConsignmentCheckBox_stateChanged(int arg1)
{
    int verticalOffset = 290;
    int windowVerticalOffset = 150;

    if (arg1 == Qt::Checked)
    {
        //move button down when consignment section is opened
        this->setFixedSize(openedSize);
        ui->CancelButton->setGeometry(ui->CancelButton->geometry().x(), ui->CancelButton->geometry().y() + verticalOffset,
                                      ui->CancelButton->geometry().width(), ui->CancelButton->geometry().height());
        ui->ConsignmentCheckBox->setGeometry(ui->ConsignmentCheckBox->geometry().x(), ui->ConsignmentCheckBox->geometry().y() + verticalOffset,
                                             ui->ConsignmentCheckBox->geometry().width(), ui->ConsignmentCheckBox->geometry().height());
        ui->SelectButton->setGeometry(ui->SelectButton->geometry().x(), ui->SelectButton->geometry().y() + verticalOffset,
                                      ui->SelectButton->geometry().width(), ui->SelectButton->geometry().height());
        ui->GroupConsignment->setVisible(true);
        this->setGeometry(this->geometry().x(), this->geometry().y() - windowVerticalOffset,
                          this->geometry().width(), this->geometry().height());

        //move the labels down when the consignment section is opened
        ui->LabelInform1->setGeometry(ui->LabelInform1->geometry().x(), ui->LabelInform1->geometry().y() + verticalOffset,
                                      ui->LabelInform1->geometry().width(), ui->LabelInform1->geometry().height());
        ui->LabelInform2->setGeometry(ui->LabelInform2->geometry().x(), ui->LabelInform2->geometry().y() + verticalOffset,
                                      ui->LabelInform2->geometry().width(), ui->LabelInform2->geometry().height());
        ui->LabelInform3->setGeometry(ui->LabelInform3->geometry().x(), ui->LabelInform3->geometry().y() + verticalOffset,
                                      ui->LabelInform3->geometry().width(), ui->LabelInform3->geometry().height());
    }
    else
    {
        //move button up when consignment section is opened
        this->setFixedSize(closedSize);
        ui->CancelButton->setGeometry(ui->CancelButton->geometry().x(), ui->CancelButton->geometry().y() - verticalOffset,
                                      ui->CancelButton->geometry().width(), ui->CancelButton->geometry().height());
        ui->ConsignmentCheckBox->setGeometry(ui->ConsignmentCheckBox->geometry().x(), ui->ConsignmentCheckBox->geometry().y() - verticalOffset,
                                             ui->ConsignmentCheckBox->geometry().width(), ui->ConsignmentCheckBox->geometry().height());
        ui->SelectButton->setGeometry(ui->SelectButton->geometry().x(), ui->SelectButton->geometry().y() - verticalOffset,
                                      ui->SelectButton->geometry().width(), ui->SelectButton->geometry().height());
        ui->GroupConsignment->setVisible(false);
        this->setGeometry(this->geometry().x(), this->geometry().y() + windowVerticalOffset,
                          this->geometry().width(), this->geometry().height());

        //move the labels up when the consignment section is opened
        ui->LabelInform1->setGeometry(ui->LabelInform1->geometry().x(), ui->LabelInform1->geometry().y() - verticalOffset,
                                      ui->LabelInform1->geometry().width(), ui->LabelInform1->geometry().height());
        ui->LabelInform2->setGeometry(ui->LabelInform2->geometry().x(), ui->LabelInform2->geometry().y() - verticalOffset,
                                      ui->LabelInform2->geometry().width(), ui->LabelInform2->geometry().height());
        ui->LabelInform3->setGeometry(ui->LabelInform3->geometry().x(), ui->LabelInform3->geometry().y() - verticalOffset,
                                      ui->LabelInform3->geometry().width(), ui->LabelInform3->geometry().height());
    }
}

void ProductSearch::on_LineSWHID_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        //implement a search function here
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE stock_id LIKE :stock_id || '%' AND saletxn_id IS NULL";
        QSqlQuery d_qry;
        d_qry.prepare(sqlStatement);
        d_qry.bindValue(":stock_id", arg1);

        if (d_qry.exec())
        {
            qDebug()<<"Attempting to find stock by WH ID...";

            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));
        }
        else
        {
            qDebug()<<d_qry.lastError();
        }

        while (d_qry.next())
        {
            //Filter the result
            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

            stockTableModel->setModel(stockModel);
            stockTableModel->resizeColumnsToContents();
            stockTableModel->horizontalHeader()->setHighlightSections(false);
            stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
            stockTableModel->horizontalHeader()->setStretchLastSection(true);
            stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
         }
    }
    else //if the user erase everything in this field, it should reset the search function
    {
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE saletxn_id IS NULL";

        QSqlQuery d_qry;

        d_qry.prepare(sqlStatement);
        d_qry.exec();

        stockModel->setQuery(d_qry);
        stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
        stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
        stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
        stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
        stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

        stockTableModel->setModel(stockModel);
        stockTableModel->resizeColumnsToContents();
        stockTableModel->horizontalHeader()->setHighlightSections(false);
        stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
        stockTableModel->horizontalHeader()->setStretchLastSection(true);
        stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
    }
}

void ProductSearch::on_LineSBrand_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        //implement a search function here
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE brand_name LIKE '%' || :brand_name || '%' AND saletxn_id IS NULL";
        QSqlQuery d_qry;
        d_qry.prepare(sqlStatement);
        d_qry.bindValue(":brand_name", arg1);

        if (d_qry.exec())
        {
            qDebug()<<"Attempting to find stock by Brand...";

            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));
        }
        else
        {
            qDebug()<<d_qry.lastError();
        }

        while (d_qry.next())
        {
            //Filter the result
            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

            stockTableModel->setModel(stockModel);
            stockTableModel->resizeColumnsToContents();
            stockTableModel->horizontalHeader()->setHighlightSections(false);
            stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
            stockTableModel->horizontalHeader()->setStretchLastSection(true);
            stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
         }
    }
    else //if the user erase everything in this field, it should reset the search function
    {
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE saletxn_id IS NULL";

        QSqlQuery d_qry;

        d_qry.prepare(sqlStatement);
        d_qry.exec();

        stockModel->setQuery(d_qry);
        stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
        stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
        stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
        stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
        stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

        stockTableModel->setModel(stockModel);
        stockTableModel->resizeColumnsToContents();
        stockTableModel->horizontalHeader()->setHighlightSections(false);
        stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
        stockTableModel->horizontalHeader()->setStretchLastSection(true);
        stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
    }
}

void ProductSearch::on_LineSModelName_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        //implement a search function here
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE model_name LIKE '%' || :model_name || '%' AND saletxn_id IS NULL";
        QSqlQuery d_qry;
        d_qry.prepare(sqlStatement);
        d_qry.bindValue(":model_name", arg1);

        if (d_qry.exec())
        {
            qDebug()<<"Attempting to find stock by Model Name...";

            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));
        }
        else
        {
            qDebug()<<d_qry.lastError();
        }

        while (d_qry.next())
        {
            //Filter the result
            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

            stockTableModel->setModel(stockModel);
            stockTableModel->resizeColumnsToContents();
            stockTableModel->horizontalHeader()->setHighlightSections(false);
            stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
            stockTableModel->horizontalHeader()->setStretchLastSection(true);
            stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
         }
    }
    else //if the user erase everything in this field, it should reset the search function
    {
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE saletxn_id IS NULL";

        QSqlQuery d_qry;

        d_qry.prepare(sqlStatement);
        d_qry.exec();

        stockModel->setQuery(d_qry);
        stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
        stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
        stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
        stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
        stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

        stockTableModel->setModel(stockModel);
        stockTableModel->resizeColumnsToContents();
        stockTableModel->horizontalHeader()->setHighlightSections(false);
        stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
        stockTableModel->horizontalHeader()->setStretchLastSection(true);
        stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
    }
}

void ProductSearch::on_LineSModelNo_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        //implement a search function here
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE model_no LIKE '%' || :model_no || '%' AND saletxn_id IS NULL";
        QSqlQuery d_qry;
        d_qry.prepare(sqlStatement);
        d_qry.bindValue(":model_no", arg1);

        if (d_qry.exec())
        {
            qDebug()<<"Attempting to find stock by Model No...";

            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));
        }
        else
        {
            qDebug()<<d_qry.lastError();
        }

        while (d_qry.next())
        {
            //Filter the result
            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

            stockTableModel->setModel(stockModel);
            stockTableModel->resizeColumnsToContents();
            stockTableModel->horizontalHeader()->setHighlightSections(false);
            stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
            stockTableModel->horizontalHeader()->setStretchLastSection(true);
            stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
         }
    }
    else //if the user erase everything in this field, it should reset the search function
    {
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE saletxn_id IS NULL";

        QSqlQuery d_qry;

        d_qry.prepare(sqlStatement);
        d_qry.exec();

        stockModel->setQuery(d_qry);
        stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
        stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
        stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
        stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
        stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

        stockTableModel->setModel(stockModel);
        stockTableModel->resizeColumnsToContents();
        stockTableModel->horizontalHeader()->setHighlightSections(false);
        stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
        stockTableModel->horizontalHeader()->setStretchLastSection(true);
        stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
    }
}

void ProductSearch::on_LineSSerialNo_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        //implement a search function here
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE serial_no LIKE '%' || :serial_no || '%' AND saletxn_id IS NULL";
        QSqlQuery d_qry;
        d_qry.prepare(sqlStatement);
        d_qry.bindValue(":serial_no", arg1);

        if (d_qry.exec())
        {
            qDebug()<<"Attempting to find stock by Serial No...";

            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));
        }
        else
        {
            qDebug()<<d_qry.lastError();
        }

        while (d_qry.next())
        {
            //Filter the result
            stockModel->setQuery(d_qry);
            stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
            stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
            stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
            stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
            stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

            stockTableModel->setModel(stockModel);
            stockTableModel->resizeColumnsToContents();
            stockTableModel->horizontalHeader()->setHighlightSections(false);
            stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
            stockTableModel->horizontalHeader()->setStretchLastSection(true);
            stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
         }
    }
    else //if the user erase everything in this field, it should reset the search function
    {
        QString sqlStatement = "SELECT 'WH' || printf('%03d', stock_id), brand_name, model_name, model_no, serial_no "
                               "FROM stockOverviewTable"
                               "    JOIN purchaseTxnTable"
                               "        USING (purchasetxn_id)"
                               "    JOIN productTable"
                               "        USING (product_id)"
                               "WHERE saletxn_id IS NULL";

        QSqlQuery d_qry;

        d_qry.prepare(sqlStatement);
        d_qry.exec();

        stockModel->setQuery(d_qry);
        stockModel->setHeaderData(0, Qt::Horizontal, tr("WH ID"));
        stockModel->setHeaderData(1, Qt::Horizontal, tr("Brand"));
        stockModel->setHeaderData(2, Qt::Horizontal, tr("Model Name"));
        stockModel->setHeaderData(3, Qt::Horizontal, tr("Model No"));
        stockModel->setHeaderData(4, Qt::Horizontal, tr("Serial No"));

        stockTableModel->setModel(stockModel);
        stockTableModel->resizeColumnsToContents();
        stockTableModel->horizontalHeader()->setHighlightSections(false);
        stockTableModel->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
        stockTableModel->horizontalHeader()->setStretchLastSection(true);
        stockTableModel->horizontalHeader()->setMinimumSectionSize(70);
    }
}

//TODO implement product search for consignment items

QString ProductSearch::getStockId(const int& i)
{
    const QModelIndexList tableList = stockTableModel->selectionModel()->selectedRows() +
            consignmentTableModel->selectionModel()->selectedRows();

    QString stockID[5];
    int rowIndex = 0;

    for (QModelIndex index : tableList)
    {
        stockID[rowIndex] = index.sibling(index.row(), 0).data().toString();
        rowIndex++;
    }
    return stockID [i];
}

int ProductSearch::getSelectedCount()
{
    int selectedCount = 0;
    const QModelIndexList stockTableList = stockTableModel->selectionModel()->selectedRows();
    const QModelIndexList consignmentTableList = consignmentTableModel->selectionModel()->selectedRows();

    for (int i = 0;i < stockTableList.count();i++)
    {
        selectedCount += 1;
    }
    for (int i = 0;i< consignmentTableList.count();i++)
    {
        selectedCount += 1;
    }

    return selectedCount; //returns the number of products entered
}

void ProductSearch::on_TableS_clicked(const QModelIndex &index)
{

    int selectedCount = getSelectedCount();

    if (selectedCount == 0 || selectedCount == 1)
    {
        ui->LabelInform2->setText(QString::number(selectedCount));
        ui->LabelInform3->setText("product");
    }
    else if (selectedCount == 6)
    {
        QMessageBox::information(this, "Input Error", "You may only select up to 5 products.");
        stockTableModel->selectionModel()->select(index, QItemSelectionModel::Deselect);
    }
    else
    {
       ui->LabelInform2->setText(QString::number(selectedCount));
       ui->LabelInform3->setText("products");
    }

}

void ProductSearch::on_TableC_clicked(const QModelIndex &index)
{
    int selectedCount = getSelectedCount();

    if (selectedCount == 0 || selectedCount == 1)
    {
        ui->LabelInform2->setText(QString::number(selectedCount));
        ui->LabelInform3->setText("product");
    }
    else if (selectedCount == 6)
    {
        QMessageBox::information(this, "Input Error", "You may only select up to 5 products.");
        consignmentTableModel->selectionModel()->select(index, QItemSelectionModel::Deselect);
    }
    else
    {
       ui->LabelInform2->setText(QString::number(selectedCount));
       ui->LabelInform3->setText("products");
    }
}

void ProductSearch::on_SelectButton_clicked()
{
    const QModelIndexList tableList = stockTableModel->selectionModel()->selectedRows() +
                            consignmentTableModel->selectionModel()->selectedRows();

    int selectedCount = 0;
    for (int i = 0;i < tableList.count();i++)
    {
        selectedCount += 1;
    }
    if (selectedCount == 0)
    {
        QMessageBox::information(this, "Input Error", "Please select product.");
    }
    else
    {
        this->accept();
    }

}


