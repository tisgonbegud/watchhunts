#include "ConsignmentEditWindow.h"
#include "ui_ConsignmentEditWindow.h"

ConsignmentEditWindow::ConsignmentEditWindow(const int& rowSelected, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConsignmentEditWindow)
{
    ui->setupUi(this);

    tableModel = ui->TableUpdate;

    itemModel = new QStandardItemModel(rowSelected, 5, this);
    itemModel->setHeaderData(0, Qt::Horizontal, "WH ID");
    itemModel->setHeaderData(1, Qt::Horizontal, "Brand");
    itemModel->setHeaderData(2, Qt::Horizontal, "Model Name");
    itemModel->setHeaderData(3, Qt::Horizontal, "Model No");
    itemModel->setHeaderData(4, Qt::Horizontal, "Serial No");
}

ConsignmentEditWindow::~ConsignmentEditWindow()
{
    delete ui;
}

void ConsignmentEditWindow::setTableData(const int& row, const int& col, const QVariant& value)
{
    itemModel->setData(itemModel->index(row, col), value);
    tableModel->setModel(itemModel);
}
