#ifndef CONSIGNMENTWINDOW_H
#define CONSIGNMENTWINDOW_H

#include <QDialog>
#include <QTableView>
#include <QKeyEvent>
#include <QMessageBox>
#include <QDebug>
#include <QStandardItemModel>
#include "ConsignmentEditWindow.h"
#include "dbManager.h"


namespace Ui {
class ConsignmentWindow;
}

class ConsignmentWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ConsignmentWindow(QWidget *parent = nullptr);
    ~ConsignmentWindow();

private slots:
    void on_addRowButton_clicked();

    void on_RemoveRowButton_clicked();

    void on_CancelButton_clicked();

    void on_OkButton_clicked();

    void keyPressEvent(QKeyEvent *e);

    void on_tabWidget_currentChanged(int index);

private:
    Ui::ConsignmentWindow *ui;

    ConsignmentEditWindow* EditWindow;

    dbManager *d_db;

    QTableView* tableReceive;
    QStandardItemModel* receiveModel;

    QTableView* tableUpdate;
    QSqlQueryModel* updateModel;

    QModelIndex itemIndex;

    void addRow();
    void removeRow();

    void receiveConsignment();
    void executeReceiveConsignment();

    void updateConsignment();
    void executeUpdateConsignment();
};

#endif // CONSIGNMENTWINDOW_H
