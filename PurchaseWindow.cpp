#include "PurchaseWindow.h"
#include "ui_PurchaseWindow.h"

PurchaseWindow::PurchaseWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PurchaseWindow)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::Dialog);
    this->setFixedSize(QSize(1277, 360));

    //install event filter to disable close by Esc Key (remember to include header QKeyEvent header file)
    this->installEventFilter(this);

    ui->RegistrationValue->setText("201920057M");

    //set up regular expression validator for the price input
    //QRegExpValidator rx("-?\\d");

    //set date to be today's date
    ui->ValueDate->setDate(QDate::currentDate());

    //set properties for TextAddress
    ui->TextAddress->setTabChangesFocus(true);

    //initialize ComboBox and insert list of options
    QStringList paymentmodelist = (QStringList()<<"Cash"<<"Cheque");
    QComboBox *paymentmodepicker = ui->PaymentModePicker;
    paymentmodepicker->addItems(paymentmodelist);


    //create database object to access data
    //first get the working directory of the executable then assign the database to a directory in database folder
    QString currentWd = QDir::currentPath();
    QString databaseWd = currentWd + "/database/db.sqlite";
    d_db = new dbManager(databaseWd);

    QSqlQuery d_qry;
    QString sqlStatement = "SELECT purchaseinvoice_id FROM purchaseInvoiceTable WHERE (SELECT MAX(purchaseinvoice_id) from purchaseInvoiceTable)";

    d_qry.prepare(sqlStatement);
    if (d_qry.exec())
    {
       qDebug()<<"Latest purchase Invoice shown";
    }
    else
    {
        qDebug()<<d_qry.lastError();
    }

    while (d_qry.next())
    {
       ui->ValueLastInvoice->setText("P-" + QString("%1").arg(d_qry.value(0).toInt(), 3, 10, QChar('0')));
    }

}

PurchaseWindow::~PurchaseWindow()
{
    delete ui;
}

void PurchaseWindow::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
    {
        e->accept(); //just accept the command and not escape from the dialog
    }
}

void PurchaseWindow::on_BackButton_clicked()
{
    bool bcustomerFilled = false;
    bool bproductFilled = false;

    if (ui->LineName->text() != "" ||
            ui->LineContactNo->text() != "" ||
            ui->LineNRIC->text() != "" ||
            ui->TextAddress->toPlainText() != ""
            )
    {
    bcustomerFilled = true;
    }

    auto productCollection = {ui->LineBrand1, ui->LineBrand2, ui->LineBrand3, ui->LineBrand4, ui->LineBrand5,
                             ui->LineModelName1, ui->LineModelName2, ui->LineModelName3, ui->LineModelName4, ui->LineModelName5,
                             ui->LineModelNo1, ui->LineModelNo2, ui->LineModelNo3, ui->LineModelNo4, ui->LineModelNo5,
                             ui->LineSerialNo1, ui->LineSerialNo2, ui->LineSerialNo3, ui->LineSerialNo4, ui->LineSerialNo5,
                             ui->LineCertificateDetails1, ui->LineCertificateDetails2, ui->LineCertificateDetails3, ui->LineCertificateDetails4, ui->LineCertificateDetails5,
                             ui->LinePrice1, ui->LinePrice2, ui->LinePrice3, ui->LinePrice4, ui->LinePrice5,
                             ui->LineRemarks1, ui->LineRemarks2, ui->LineRemarks3, ui->LineRemarks4, ui->LineRemarks5};

    int filledCount = 0;

    for (auto field : productCollection)
    {
        if (field->text() != "")
        {
            filledCount += 1;
        }
    }

    if (filledCount > 0)
    {
        bproductFilled = true;
    }

    if (bcustomerFilled && !bproductFilled)
    {
        int ans = QMessageBox::warning(this, "Customer Details Found", "You have entered some customer data.\n\nAre you sure you want to go back?\n\n(You will lose unsaved changes)", QMessageBox::No|QMessageBox::Yes);
             if (ans == QMessageBox::Yes)
             {
                  this->reject();
             }
             else
             {
                 return;
             }
    }
    else if (bproductFilled && !bcustomerFilled)
    {
        int ans = QMessageBox::warning(this, "Product Details Found", "You have entered some product data.\n\nAre you sure you want to go back?\n\n(You will lose unsaved changes)", QMessageBox::No|QMessageBox::Yes);
             if (ans == QMessageBox::Yes)
             {
                  this->reject();
             }
             else
             {
                 return;
             }
    }
    else if(bproductFilled && bcustomerFilled)
    {
        int ans = QMessageBox::warning(this, "Customer And Product Details Found", "You have entered some customer and product data.\n\nAre you sure you want to go back?\n\n(You will lose unsaved changes)", QMessageBox::No|QMessageBox::Yes);
             if (ans == QMessageBox::Yes)
             {
                  this->reject();
             }
             else
             {
                 return;
             }
    }
    else
    {
        //close the dialog window if the every detail is blank
        this->reject();
    }

}

void PurchaseWindow::on_PaymentModePicker_currentTextChanged(const QString &arg1)
{
    if (arg1 == "Cheque")
    {
        ui->PaymentModeDetailsGroup->resize(300, ui->PaymentModeDetailsGroup->height());
        ui->LabelPaymentModeNo->show();
        ui->LabelPaymentModeNo->setText("Cheque No :");
        ui->LinePaymentModeNo->show();

    }
    else
    {
        ui->PaymentModeDetailsGroup->resize(160, ui->PaymentModeDetailsGroup->height());
        ui->LabelPaymentModeNo->hide();
        ui->LinePaymentModeNo->hide();
    }
}

void PurchaseWindow::on_CustomerSearchButton_clicked()
{
    if (ui->LineName->text() != "" || ui->LineContactNo->text() != "" || ui->LineNRIC->text() != ""
            || ui->TextAddress->toPlainText() != "")
    {
    int answer = QMessageBox::warning(this, "Clear Customer Details", "You have entered some customer's details.\nAll unsaved data will be lost.\n\nAre you sure?",
                         QMessageBox::No | QMessageBox::Yes);
        if (answer == QMessageBox::Yes)
            {
                //clear the customer details when user click on clear
                ui->LineName->clear();
                ui->LineContactNo->clear();
                ui->LineNRIC->clear();
                ui->TextAddress->clear();

                //disable the text field again after clearing
                ui->LineName->setEnabled(false);
                ui->LineContactNo->setEnabled(false);
                ui->LineNRIC->setEnabled(false);
                ui->TextAddress->setEnabled(false);

                //enable add new customer button if user wants to search
                ui->AddNewCustomerButton->setEnabled(true);

                customerSearchWin = new CustomerSearch();
                customerSearchWin->setModal(true);

                //if customerSearchWin exec returns 1, get data from the customer search window
                if (customerSearchWin->exec())
                {
                    ui->LineName->setText(customerSearchWin->getName());
                    ui->LineContactNo->setText(customerSearchWin->getContactNo());
                    ui->LineNRIC->setText(customerSearchWin->getNricNo());
                    ui->TextAddress->setText(customerSearchWin->getAddress());
                    ui->EditButton->setEnabled(true);
                }
                //else do nothing and just print a message
                else if (customerSearchWin->result() == QDialog::Rejected)
                {
                    qDebug()<<"Customer search operation cancelled by user";
                }

            }
     }
    else
    {
        //disable the text field again if the fields are already empty to prevent input after searching
        ui->LineName->setEnabled(false);
        ui->LineContactNo->setEnabled(false);
        ui->LineNRIC->setEnabled(false);
        ui->TextAddress->setEnabled(false);

        //enable add new customer button if user wants to search
        ui->AddNewCustomerButton->setEnabled(true);

        customerSearchWin = new CustomerSearch();
        customerSearchWin->setModal(true);

        //if customerSearchWin exec returns 1, get data from the customer search window
        if (customerSearchWin->exec())
        {
            ui->LineName->setText(customerSearchWin->getName());
            ui->LineContactNo->setText(customerSearchWin->getContactNo());
            ui->LineNRIC->setText(customerSearchWin->getNricNo());
            ui->TextAddress->setText(customerSearchWin->getAddress());
            ui->EditButton->setEnabled(true);
        }
        //else do nothing and just print a message
        else if (customerSearchWin->result() == QDialog::Rejected)
        {
            qDebug()<<"Customer search operation cancelled by user";
        }
    }
}

void PurchaseWindow::on_CustomerClearButton_clicked()
{
    if (ui->LineName->text() != "" || ui->LineContactNo->text() != "" || ui->LineNRIC->text() != ""
            || ui->TextAddress->toPlainText() != "")
    {
    int answer = QMessageBox::warning(this, "Clear Customer Details", "Customer data will be cleared and you will lose unsaved changes.\n\nAre you sure?",
                         QMessageBox::No | QMessageBox::Yes);
        if (answer == QMessageBox::Yes)
            {
                //clear the customer details when user click on clear
                ui->LineName->clear();
                ui->LineContactNo->clear();
                ui->LineNRIC->clear();
                ui->TextAddress->clear();

                //set focus on the first first box after clearing for ease of typing
                ui->LineName->setFocus();

                //disable the edit button if it's turn on. Otherwise no net effect
                ui->EditButton->setEnabled(false);
            }
    }
}

void PurchaseWindow::on_AddNewCustomerButton_clicked()
{
    if (ui->LineName->isEnabled() == false &&
            ui->LineContactNo->isEnabled() == false &&
            ui->LineNRIC->isEnabled() == false &&
            ui->TextAddress->isEnabled() == false)
    {
        if (ui->LineName->text() == "" &&
                ui->LineContactNo->text() == "" &&
                ui->LineNRIC->text() == "" &&
                ui->TextAddress->toPlainText() == "")
        {
            //disable the add customer button so users cannot click again
            ui->AddNewCustomerButton->setEnabled(false);

            //enable the textbox when user click on add customer button
            ui->LineName->setDisabled(false);
            ui->LineContactNo->setDisabled(false);
            ui->LineNRIC->setDisabled(false);
            ui->TextAddress->setDisabled(false);

            //set focus on the first box for ease of typing
            ui->LineName->setFocus();
        }
        else
        {
            int ans = QMessageBox::warning(this, "Customer Details Found", "Customer details currently in the form will be cleared.\n\nAre you sure?", QMessageBox::No|QMessageBox::Yes);
                if (ans == QMessageBox::Yes)
                {
                    //disable the add customer button so users cannot click again
                    ui->AddNewCustomerButton->setEnabled(false);

                    //clear all data in the textbox first
                    ui->LineName->clear();
                    ui->LineContactNo->clear();
                    ui->LineNRIC->clear();
                    ui->TextAddress->clear();

                    //enable the textbox when user click on add customer button
                    ui->LineName->setDisabled(false);
                    ui->LineContactNo->setDisabled(false);
                    ui->LineNRIC->setDisabled(false);
                    ui->TextAddress->setDisabled(false);

                    //set focus on the first box for ease of typing
                    ui->LineName->setFocus();
                }
        }
    }
}

void PurchaseWindow::on_LineName_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        ui->CustomerClearButton->setEnabled(true);
    }
    else if(arg1 == "" && ui->LineContactNo->text() == "" && ui->LineNRIC->text() == "" && ui->TextAddress->toPlainText() == "")
    {
        ui->CustomerClearButton->setEnabled(false);
    }
}

void PurchaseWindow::on_LineContactNo_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        ui->CustomerClearButton->setEnabled(true);
    }
    else if(arg1 == "" && ui->LineContactNo->text() == "" && ui->LineNRIC->text() == "" && ui->TextAddress->toPlainText() == "")
    {
        ui->CustomerClearButton->setEnabled(false);
    }
}

void PurchaseWindow::on_LineNRIC_textChanged(const QString &arg1)
{
    if (arg1 != "")
    {
        ui->CustomerClearButton->setEnabled(true);
    }
    else if(arg1 == "" && ui->LineContactNo->text() == "" && ui->LineNRIC->text() == "" && ui->TextAddress->toPlainText() == "")
    {
        ui->CustomerClearButton->setEnabled(false);
    }
}

void PurchaseWindow::on_TextAddress_textChanged()
{
    if (ui->TextAddress->toPlainText() != "")
    {
        ui->CustomerClearButton->setEnabled(true);
    }
    else if(ui->LineName->text() == "" && ui->LineContactNo->text() == "" && ui->LineNRIC->text() == "" && ui->TextAddress->toPlainText() == "")
    {
        ui->CustomerClearButton->setEnabled(false);
    }
}

void PurchaseWindow::on_ProductClearButton_clicked()
{
    auto productCollection = {ui->LineBrand1, ui->LineBrand2, ui->LineBrand3, ui->LineBrand4, ui->LineBrand5,
                             ui->LineModelName1, ui->LineModelName2, ui->LineModelName3, ui->LineModelName4, ui->LineModelName5,
                             ui->LineModelNo1, ui->LineModelNo2, ui->LineModelNo3, ui->LineModelNo4, ui->LineModelNo5,
                             ui->LineSerialNo1, ui->LineSerialNo2, ui->LineSerialNo3, ui->LineSerialNo4, ui->LineSerialNo5,
                             ui->LineCertificateDetails1, ui->LineCertificateDetails2, ui->LineCertificateDetails3, ui->LineCertificateDetails4, ui->LineCertificateDetails5,
                             ui->LinePrice1, ui->LinePrice2, ui->LinePrice3, ui->LinePrice4, ui->LinePrice5,
                             ui->LineRemarks1, ui->LineRemarks2, ui->LineRemarks3, ui->LineRemarks4, ui->LineRemarks5};

   int productFilledCount = 0;

    for (auto field: productCollection)
    {
        if (field->text() != "")
        {
            productFilledCount += 1;
        }

    }

    if (productFilledCount > 0)
    {
        int answer = QMessageBox::warning(this, "Product Details Found", "Product data will be cleared and you will lose unsaved changes.\n\nAre you sure?", QMessageBox::No|QMessageBox::Yes);
            if (answer == QMessageBox::Yes)
            {
                for (auto field : productCollection)
                {
                    field->clear();
                    ui->ProductClearButton->setEnabled(false);
                }
            }

            //Set the focus on the first line in product details when the clear button is clicked
            ui->LineBrand1->setFocus();
    }
}

void PurchaseWindow::productClearButton_enabler(const QString &arg1)
{
    auto productCollection = {ui->LineBrand1, ui->LineBrand2, ui->LineBrand3, ui->LineBrand4, ui->LineBrand5,
                              ui->LineModelName1, ui->LineModelName2, ui->LineModelName3, ui->LineModelName4, ui->LineModelName5,
                              ui->LineModelNo1, ui->LineModelNo2, ui->LineModelNo3, ui->LineModelNo4, ui->LineModelNo5,
                              ui->LineSerialNo1, ui->LineSerialNo2, ui->LineSerialNo3, ui->LineSerialNo4, ui->LineSerialNo5,
                              ui->LineCertificateDetails1, ui->LineCertificateDetails2, ui->LineCertificateDetails3, ui->LineCertificateDetails4, ui->LineCertificateDetails5,
                              ui->LinePrice1, ui->LinePrice2, ui->LinePrice3, ui->LinePrice4, ui->LinePrice5,
                              ui->LineRemarks1, ui->LineRemarks2, ui->LineRemarks3, ui->LineRemarks4, ui->LineRemarks5};

    for (auto field : productCollection)
    {
        field->text() = arg1;

        if (field->text() != "")
        {
            //just enable the clear button once
            ui->ProductClearButton->setEnabled(true);
            return;
        }
        else
        {
            ui->ProductClearButton->setEnabled(false);
        }
    }
}

void PurchaseWindow::on_LineBrand1_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineBrand2_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineBrand3_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineBrand4_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineBrand5_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelName1_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelName2_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelName3_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelName4_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelName5_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelNo1_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelNo2_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelNo3_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelNo4_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineModelNo5_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineSerialNo1_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineSerialNo2_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineSerialNo3_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineSerialNo4_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineSerialNo5_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineCertificateDetails1_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineCertificateDetails2_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineCertificateDetails3_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineCertificateDetails4_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineCertificateDetails5_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LinePrice1_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LinePrice2_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LinePrice3_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LinePrice4_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LinePrice5_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineRemarks1_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineRemarks2_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineRemarks3_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineRemarks4_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_LineRemarks5_textChanged(const QString &arg1)
{
    productClearButton_enabler(arg1);
}

void PurchaseWindow::on_OkButton_clicked()
{
    QString msgboxtitle = "Incomplete Customer Details";

    //set the default style sheet for generic message box
    QString msgboxstylesheet = "QMessageBox{"
                               "background-color: rgb(255, 255,255);"
                               "padding-top: 2px;"
                               "}"
                               "QPixmap{"
                               "padding-left: 2px;"
                               "}"
                               ""
                               "QPushButton{"
                               "border-style: solid;"
                               "border-width: 1px;"
                               "border-radius: 10px;"
                               "border-color: rgb(169, 169, 169);"
                               "background-color: rgb(255, 255, 255);"
                               "min-width: 5em;"
                               "min-height: 1em;"
                               "padding: 6px;"
                               "} "

                               "QPushButton:hover{"
                               "border-style: solid;"
                               "border-width: 1px;"
                               "border-radius: 10px;"
                               "border-color: rgb(134, 191, 160);"
                               "background-color: rgb(255, 239, 133);" //211, 240, 224
                               "min-width: 5em;"
                               "min-height: 1em;"
                               "padding: 6px;"
                               "}";

    if (ui->LineName->text() == "")
    {
        QString msgtext = "Please enter the customer's name.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->LineName->setFocus();
        return;
    }
    else if (ui->LineContactNo->text() == "")
    {
        QString msgtext = "Please enter the customer's contact number.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->LineContactNo->setFocus();
        return;
    }
    else if(ui->LineNRIC->text() == "")
    {
        QString msgtext = "Please enter the customer's NRIC/Passport number.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->LineNRIC->setFocus();
        return;
    }
    else if(ui->TextAddress->toPlainText()== "")
    {
        QString msgtext = "Please enter the customer's address.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->TextAddress->setFocus();
        return;
    }
    else if (ui->PaymentModePicker->currentText() == "Cheque" && ui->LinePaymentModeNo->text() == "")
    {
        QString msgtext = "Please enter cheque number.";
        QMessageBox msgbox;
        msgbox.setWindowTitle(msgboxtitle);
        msgbox.setText(msgtext);
        msgbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        msgbox.setStyleSheet(msgboxstylesheet);

        msgbox.exec();
        ui->LinePaymentModeNo->setFocus();
        return;
    }

    bool rowBeginFill[5] = {0};
    int rowFilledCount[5] = {false};

    auto row1 = {ui->LineBrand1, ui->LineModelName1, ui->LineModelNo1, ui->LineSerialNo1, ui->LineCertificateDetails1, ui->LinePrice1, ui->LineRemarks1};
    auto row2 = {ui->LineBrand2, ui->LineModelName2, ui->LineModelNo2, ui->LineSerialNo2, ui->LineCertificateDetails2, ui->LinePrice2, ui->LineRemarks2};
    auto row3 = {ui->LineBrand3, ui->LineModelName3, ui->LineModelNo3, ui->LineSerialNo3, ui->LineCertificateDetails3, ui->LinePrice3, ui->LineRemarks3};
    auto row4 = {ui->LineBrand4, ui->LineModelName4, ui->LineModelNo4, ui->LineSerialNo4, ui->LineCertificateDetails4, ui->LinePrice4, ui->LineRemarks4};
    auto row5 = {ui->LineBrand5, ui->LineModelName5, ui->LineModelNo5, ui->LineSerialNo5, ui->LineCertificateDetails5, ui->LinePrice5, ui->LineRemarks5};

    for (auto field : row1)
    {
        if (field->text() != "")
        {
            rowBeginFill[0] = true;
            rowFilledCount[0] += 1;
        }
    }

    for (auto field : row2)
    {
        if (field->text() != "")
        {
            rowBeginFill[1] = true;
            rowFilledCount[1] += 1;
        }
    }

    for (auto field : row3)
    {
        if (field->text() != "")
        {
            rowBeginFill[2] = true;
            rowFilledCount[2] += 1;
        }
    }

    for (auto field : row4)
    {
        if (field->text() != "")
        {
            rowBeginFill[3] = true;
            rowFilledCount[3] += 1;
        }
    }

    for (auto field : row5)
    {
        if (field->text() != "")
        {
            rowBeginFill[4] = true;
            rowFilledCount[4] += 1;
        }
    }

    QMessageBox warningbox;
    int filledRowCount = 0;

    for (int i = 0; i <= 4; i++)
    {

        if (rowBeginFill[i] == true && rowFilledCount[i] == 7)
        {
            filledRowCount += 1;
        }
        else if (rowBeginFill[i] == true && rowFilledCount[i] < 7)
        {
            QString warningmsg = QStringLiteral("Row %1 is incomplete. Please check all the fields in row %1.").arg(i + 1);
            warningbox.setText(warningmsg);
            warningbox.setWindowTitle("Input Error");
            warningbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
            warningbox.setStyleSheet(msgboxstylesheet);

            warningbox.exec();

            return;
         }
    }

    if (filledRowCount == 0)
    {
        QString warningmsg = "Please enter at least one product.";
        warningbox.setText(warningmsg);
        warningbox.setWindowTitle("Input Error");
        warningbox.setIconPixmap(QPixmap(":/Halt Icon.png"));
        warningbox.setStyleSheet(msgboxstylesheet);

        warningbox.exec();

        return;
    }

    //if all the customer's details are enabled for editing, it means it is a new customer
    if (ui->LineName->isEnabled() && ui->LineContactNo->isEnabled() &&
            ui->LineNRIC->isEnabled() && ui->TextAddress->isEnabled())
    {
        d_db->addNewCustomer(ui->LineName->text(), ui->LineContactNo->text(),
                             ui->LineNRIC->text(), ui->TextAddress->toPlainText());
    }

    executeRecordTransaction();
    QMessageBox::information(this, "Operation Success", "Purchase was successfully recorded.");
    this->accept();
}

void PurchaseWindow::executeRecordTransaction()
{
    QLineEdit *brandIndex[5] = {ui->LineBrand1, ui->LineBrand2, ui->LineBrand3, ui->LineBrand4, ui->LineBrand5};
    QLineEdit *modelNameIndex[5] = {ui->LineModelName1, ui->LineModelName2, ui->LineModelName3, ui->LineModelName4, ui->LineModelName5};
    QLineEdit *modelNoIndex[5] = {ui->LineModelNo1, ui->LineModelNo2, ui->LineModelNo3, ui->LineModelNo4, ui->LineModelNo5};
    QLineEdit *serialNoIndex[5] = {ui->LineSerialNo1, ui->LineSerialNo2, ui->LineSerialNo3, ui->LineSerialNo4, ui->LineSerialNo5};
    QLineEdit *certDetailsIndex[5] = {ui->LineCertificateDetails1, ui->LineCertificateDetails2, ui->LineCertificateDetails3,
                            ui->LineCertificateDetails4, ui->LineCertificateDetails5};
    QLineEdit *costPriceIndex[5] = {ui->LinePrice1, ui->LinePrice2, ui->LinePrice3, ui->LinePrice4, ui->LinePrice5};
    QLineEdit *remarksIndex[5] = {ui->LineRemarks1, ui->LineRemarks2, ui->LineRemarks3, ui->LineRemarks4, ui->LineRemarks5};

    int customer_id;
    int product_id;
    int purchasetxn_id;
    int purchaseInvoice_id;

    //execute sql query first to select the customer_id
    QString sqlStatement = "SELECT customer_id FROM customersTable WHERE name = :name";

    QSqlQuery d_qry;
    d_qry.prepare(sqlStatement);
    d_qry.bindValue(":name", ui->LineName->text());

    if (d_qry.exec())
    {
        qDebug()<<"customer_id query success!";
    }
    else
    {
        qDebug()<<"customer id error :"<<d_qry.lastError();
    }
    while(d_qry.next())
    {
        customer_id = d_qry.value(0).toInt();
    }

    //create a new purchase invoice (before the loop because we only need one invoice number
    d_db->addPurchaseInvoice(ui->ValueDate->text());

    //iterate through the first column to determine which row has data,
    //if the cell is filled, the rest of the data in the same row will be added into product table
    for (int i = 0; i <= 4; i++)
    {
        if(brandIndex[i]->text() != "")
        {
            d_db->addNewProduct(brandIndex[i]->text(), modelNameIndex[i]->text(), modelNoIndex[i]->text().toUpper(),
                                serialNoIndex[i]->text().toUpper(), certDetailsIndex[i]->text());

                //do another sql query to find the latest added product id from above
                sqlStatement = "SELECT product_id FROM productTable WHERE (SELECT MAX(product_id) from productTable)";
                d_qry.prepare(sqlStatement);
                if (d_qry.exec())
                {
                    qDebug()<<"product_id query success!";
                }
                else
                {
                    qDebug()<<d_qry.lastError();
                }

                while (d_qry.next())
                {
                   product_id = d_qry.value(0).toInt();
                }

            //add purchase transaction in the same loop, passing in the product_id from the sql query
                if (ui->PaymentModePicker->currentText() == "Cheque"){
            d_db->addPurchaseTxn(costPriceIndex[i]->text().toFloat(), ui->ValueDate->text(), ui->PaymentModePicker->currentText(), ui->LinePaymentModeNo->text(), remarksIndex[i]->text(), customer_id, product_id);
                }
                else if (ui->PaymentModePicker->currentText() == "Cash"){
            d_db->addPurchaseTxn(costPriceIndex[i]->text().toFloat(), ui->ValueDate->text(), ui->PaymentModePicker->currentText(), "NA", remarksIndex[i]->text(), customer_id, product_id);
                }

                //do another sql query to find the latest added purchase invoice to insert into the stock overview table
                sqlStatement = "SELECT purchasetxn_id FROM purchaseTxnTable WHERE (SELECT MAX(purchasetxn_id) from purchaseTxnTable)";
                d_qry.prepare(sqlStatement);
                if (d_qry.exec())
                {
                    qDebug()<<"purchasetxn_id query success!";
                }
                else
                {
                    qDebug()<<d_qry.lastError();
                }

                while (d_qry.next())
                {
                   purchasetxn_id = d_qry.value(0).toInt();
                }

              d_db->addPurchaseStockOverview(purchasetxn_id);

                //do another sql query to find the latest added purchase invoice to insert into the bridging table
                sqlStatement = "SELECT purchaseinvoice_id FROM purchaseInvoiceTable WHERE (SELECT MAX(purchaseinvoice_id) from purchaseInvoiceTable)";
                d_qry.prepare(sqlStatement);
                if (d_qry.exec())
                {
                    qDebug()<<"purchaseinvoice_id query success!";
                }
                else
                {
                    qDebug()<<d_qry.lastError();
                }

                while (d_qry.next())
                {
                   purchaseInvoice_id = d_qry.value(0).toInt();
                }


             //add the newly added transaction and invoice into a bridging table
             d_db->addPurchaseTxnInvoice(purchaseInvoice_id);
        }
    }
}
