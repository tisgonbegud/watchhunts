#ifndef CONSIGNMENTPAYMENTWINDOW_H
#define CONSIGNMENTPAYMENTWINDOW_H

#include <QDialog>

namespace Ui {
class ConsignmentPaymentWindow;
}

class ConsignmentPaymentWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ConsignmentPaymentWindow(QWidget *parent = nullptr);
    ~ConsignmentPaymentWindow();

private:
    Ui::ConsignmentPaymentWindow *ui;
};

#endif // CONSIGNMENTPAYMENTWINDOW_H
