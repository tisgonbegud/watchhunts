#ifndef PURCHASEWINDOW_H
#define PURCHASEWINDOW_H

#include <QDialog>
#include <QDate>
#include <QKeyEvent>
#include <QMessageBox>
#include <QPixmap>
#include <QRegExpValidator>
#include "dbManager.h"
#include "CustomerSearch.h"

namespace Ui {
class PurchaseWindow;
}

class PurchaseWindow : public QDialog
{
    Q_OBJECT

public:
    explicit PurchaseWindow(QWidget *parent = nullptr);
    ~PurchaseWindow();

private slots:
    void on_BackButton_clicked();

    void keyPressEvent(QKeyEvent *e);

    void on_PaymentModePicker_currentTextChanged(const QString &arg1);

    void on_CustomerSearchButton_clicked();

    void on_AddNewCustomerButton_clicked();

    void on_CustomerClearButton_clicked();

    void on_LineName_textChanged(const QString &arg1);

    void on_LineContactNo_textChanged(const QString &arg1);

    void on_LineNRIC_textChanged(const QString &arg1);

    void on_TextAddress_textChanged();

    void on_ProductClearButton_clicked();

    void on_LineBrand1_textChanged(const QString &arg1);

    void on_LineBrand2_textChanged(const QString &arg1);

    void on_LineBrand3_textChanged(const QString &arg1);

    void on_LineBrand4_textChanged(const QString &arg1);

    void on_LineBrand5_textChanged(const QString &arg1);

    void on_LineModelName1_textChanged(const QString &arg1);

    void on_LineModelName2_textChanged(const QString &arg1);

    void on_LineModelName3_textChanged(const QString &arg1);

    void on_LineModelName4_textChanged(const QString &arg1);

    void on_LineModelName5_textChanged(const QString &arg1);

    void on_LineModelNo1_textChanged(const QString &arg1);

    void on_LineModelNo2_textChanged(const QString &arg1);

    void on_LineModelNo3_textChanged(const QString &arg1);

    void on_LineModelNo4_textChanged(const QString &arg1);

    void on_LineModelNo5_textChanged(const QString &arg1);

    void on_LineSerialNo1_textChanged(const QString &arg1);

    void on_LineSerialNo2_textChanged(const QString &arg1);

    void on_LineSerialNo3_textChanged(const QString &arg1);

    void on_LineSerialNo4_textChanged(const QString &arg1);

    void on_LineSerialNo5_textChanged(const QString &arg1);

    void on_LineCertificateDetails1_textChanged(const QString &arg1);

    void on_LineCertificateDetails2_textChanged(const QString &arg1);

    void on_LineCertificateDetails3_textChanged(const QString &arg1);

    void on_LineCertificateDetails4_textChanged(const QString &arg1);

    void on_LineCertificateDetails5_textChanged(const QString &arg1);

    void on_LinePrice1_textChanged(const QString &arg1);

    void on_LinePrice2_textChanged(const QString &arg1);

    void on_LinePrice3_textChanged(const QString &arg1);

    void on_LinePrice4_textChanged(const QString &arg1);

    void on_LinePrice5_textChanged(const QString &arg1);

    void on_LineRemarks1_textChanged(const QString &arg1);

    void on_LineRemarks2_textChanged(const QString &arg1);

    void on_LineRemarks3_textChanged(const QString &arg1);

    void on_LineRemarks4_textChanged(const QString &arg1);

    void on_LineRemarks5_textChanged(const QString &arg1);

    void on_OkButton_clicked();

private:
    Ui::PurchaseWindow *ui;

    void productClearButton_enabler(const QString &arg1);

    void executeRecordTransaction();

    dbManager *d_db;

    CustomerSearch *customerSearchWin;

};

#endif // PURCHASEWINDOW_H
