#ifndef WATCHHUNTS_H
#define WATCHHUNTS_H

#include <QMainWindow>
#include "SaleWindow.h"
#include "PurchaseWindow.h"
#include "ConsignmentWindow.h"
#include "ViewDatabase.h"
#include <QtSql>

namespace Ui {
class WatchHunts;
}

class WatchHunts : public QMainWindow
{
    Q_OBJECT

public:
    explicit WatchHunts(QWidget *parent = nullptr);
    ~WatchHunts();

private slots:
    void on_NewSalesButton_clicked();

    void on_NewPurchaseButton_clicked();

    void createAndConnectToDatabase();

    void on_ViewStockButton_clicked();

    void on_ViewCustomerButton_clicked();

    void on_ConsignmentButton_clicked();

    void on_ViewConsignmentButton_clicked();

    void on_TransactionOkButton_clicked();

private:
    Ui::WatchHunts *ui;

    dbManager *w_db;

    QComboBox* transactionPicker;
    QComboBox* transactionYearPicker;

    PurchaseWindow *purchaseWin;
    SaleWindow *saleWin;
    ConsignmentWindow *consignmentWin;
    ViewDatabase *databaseWin;

};

#endif // WATCHHUNTS_H
