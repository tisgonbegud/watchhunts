#include "dbManager.h"

dbManager::dbManager(const QString& dbPath)
{
    const QString DRIVER("QSQLITE");

    //create the database and set it to the default directory if it does not exist
    //otherwise just set the database to the existing file

    if (!m_db.isValid())
    {
        if (QSqlDatabase::isDriverAvailable(DRIVER))
        {
            m_db = QSqlDatabase::addDatabase(DRIVER);
            m_db.setDatabaseName(dbPath);
        }
    }

    if (!m_db.open())
    {
        qDebug()<<m_db.lastError();
    }
    else if (m_db.open())
    {
        qDebug()<<"Database connected";
    }

}

void dbManager::createCustomerTable()
{
    QSqlQuery m_qry;
    QString sqlStatement;
    sqlStatement = "CREATE TABLE IF NOT EXISTS customersTable ("
                   "customer_id integer PRIMARY KEY,"
                   "name TEXT NOT NULL,"
                   "contact_no TEXT NOT NULL,"
                   "nric_no TEXT NOT NULL,"
                   "address TEXT NOT NULL,"
                   "UNIQUE (name, nric_no)"
                   ");";

    m_qry.prepare(sqlStatement);
    if (m_qry.exec())
    {
        qDebug()<<"Customer table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }

    return;
}

void dbManager::addNewCustomer(const QString &name, const QString &contact_no, const QString &nric_no, const QString &address)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO customersTable ("
                           "name,"
                           "contact_no,"
                           "nric_no,"
                           "address"
                           ")"
                           "VALUES ("
                           ":name,"
                           ":contact_no,"
                           ":nric_no,"
                           ":address"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":name", name);
    m_qry.bindValue(":contact_no", contact_no);
    m_qry.bindValue(":nric_no", nric_no);
    m_qry.bindValue(":address", address);

    if (m_qry.exec())
    {
        qDebug()<<"Customer data inserted";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }

    return;
}

void dbManager::createProductTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS productTable ("
                           "product_id INTEGER PRIMARY KEY,"
                           "brand_name TEXT NOT NULL,"
                           "model_name TEXT NOT NULL,"
                           "model_no TEXT NOT NULL,"
                           "serial_no TEXT NOT NULL,"
                           "cert_details TEXT NOT NULL"
                           ");";
    m_qry.prepare(sqlStatement);

    if(m_qry.exec())
    {
        qDebug()<<"Stock table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }

    return;
}

void dbManager::addNewProduct(const QString &brand_name,
                              const QString &model_name, const QString &model_no,
                              const QString &serial_no, const QString &cert_details)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO productTable ("
                           "brand_name,"
                           "model_name,"
                           "model_no,"
                           "serial_no,"
                           "cert_details"
                           ")"
                           "VALUES ("
                           ":brand_name,"
                           ":model_name,"
                           ":model_no,"
                           ":serial_no,"
                           ":cert_details"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":brand_name", brand_name);
    m_qry.bindValue(":model_name", model_name);
    m_qry.bindValue(":model_no", model_no);
    m_qry.bindValue(":serial_no", serial_no);
    m_qry.bindValue(":cert_details", cert_details);

    if (m_qry.exec())
    {
        qDebug()<<"Product data added";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}



void dbManager::createPurchaseTxnTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS purchaseTxnTable ("
                          "purchasetxn_id INTEGER PRIMARY KEY,"
                          "cost_price REAL NOT NULL,"
                          "date_purchased TEXT NOT NULL,"
                          "purchase_paymentmethod TEXT NOT NULL,"
                          "purchase_cheque_no TEXT,"
                          "purchase_remarks TEXT NOT NULL,"
                          "purchase_customer_id INTEGER NOT NULL,"
                          "product_id INTEGER NOT NULL,"
                          "FOREIGN KEY (purchase_customer_id) REFERENCES customersTable (customer_id)"
                          "FOREIGN KEY (product_id) REFERENCES productTable (product_id)"
                          ");";
    m_qry.prepare(sqlStatement);
    if (m_qry.exec())
    {
        qDebug()<<"Purchase transaction table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
    return;
}

void dbManager::addPurchaseTxn(const float& cost_price, const QString& date_purchased, const QString& purchase_paymentmethod,
                               const QString& purchase_cheque_no, const QString& purchase_remarks,
                               const int& purchase_customer_id, const int& product_id)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO purchaseTxnTable ("
                           "cost_price,"
                           "date_purchased,"
                           "purchase_paymentmethod,"
                           "purchase_cheque_no,"
                           "purchase_remarks,"
                           "purchase_customer_id,"
                           "product_id"
                           ")"
                           "VALUES ("
                           ":cost_price,"
                           ":date_purchased,"
                           ":purchase_paymentmethod,"
                           ":purchase_cheque_no,"
                           ":purchase_remarks,"
                           ":purchase_customer_id,"
                           ":product_id"
                           ");";

    //no need to query customer_id because it is done by the function caller and because customer_id is not part of the loop

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":cost_price", cost_price);
    m_qry.bindValue(":date_purchased", date_purchased);
    m_qry.bindValue(":purchase_paymentmethod", purchase_paymentmethod);
    m_qry.bindValue(":purchase_cheque_no", purchase_cheque_no);
    m_qry.bindValue(":purchase_remarks", purchase_remarks);
    m_qry.bindValue(":purchase_customer_id", purchase_customer_id);
    m_qry.bindValue(":product_id", product_id);

    if (m_qry.exec())
    {
        qDebug()<<"Product transaction data added";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createPurchaseInvoiceTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS purchaseInvoiceTable ("
                           "purchaseinvoice_id INTEGER PRIMARY KEY,"
                           "date TEXT NOT NULL"
                           ");";

    m_qry.prepare(sqlStatement);
    if (m_qry.exec())
    {
        qDebug()<<"Purchase invoice table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addPurchaseInvoice(const QString& date)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO purchaseInvoiceTable ("
                           "date"
                           ")"
                           "VALUES ("
                           ":date"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":date", date);
    if (m_qry.exec())
    {
        qDebug()<<"Purchase invoice data added";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createPurchaseTxnInvoiceTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS purchaseTxnInvoiceTable ("
                           "purchasetxn_id INTEGER PRIMARY KEY,"
                           "purchaseinvoice_id INTEGER,"
                           "FOREIGN KEY (purchaseinvoice_id) REFERENCES purchaseInvoiceTable (purchaseinvoice_id)"
                           ");";

    m_qry.prepare(sqlStatement);
    if (m_qry.exec())
    {
        qDebug()<<"Purchase transaction/invoice bridging table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addPurchaseTxnInvoice(const int& purchaseinvoice_id)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO purchaseTxnInvoiceTable ("
                           "purchaseinvoice_id"
                           ")"
                           "VALUES ("
                           ":purchaseinvoice_id"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":purchaseinvoice_id", purchaseinvoice_id);

    if (m_qry.exec())
    {
        qDebug()<<"Purchase bridging table updated";
    }
    else {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createSaleTxnTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS saleTxnTable ("
                          "saletxn_id INTEGER PRIMARY KEY,"
                          "selling_price REAL NOT NULL,"
                          "date_sold TEXT NOT NULL,"
                          "sale_paymentmethod TEXT NOT NULL,"
                          "sale_cheque_no TEXT,"
                          "last3_4digits TEXT,"
                          "sale_remarks TEXT NOT NULL,"
                          "sale_customer_id INTEGER NOT NULL,"
                          "product_id INTEGER NOT NULL,"
                          "FOREIGN KEY (sale_customer_id) REFERENCES customersTable (customer_id)"
                          "FOREIGN KEY (product_id) REFERENCES productTable (product_id)"
                          ");";
    m_qry.prepare(sqlStatement);
    if (m_qry.exec())
    {
        qDebug()<<"Sale transaction table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
    return;
}

void dbManager::addSaleTxn(const float& selling_price, const QString& date_sold, const QString& sale_paymentmethod,
                           const QString &sale_cheque_no, const QString& last3_4digits, const QString& sale_remarks,
                           const int& sale_customer_id, const int& product_id)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO saleTxnTable ("
                           "selling_price,"
                           "date_sold,"
                           "sale_paymentmethod,"
                           "sale_cheque_no,"
                           "last3_4digits,"
                           "sale_remarks,"
                           "sale_customer_id,"
                           "product_id"
                           ")"
                           "VALUES ("
                           ":selling_price,"
                           ":date_sold,"
                           ":sale_paymentmethod,"
                           ":sale_cheque_no,"
                           ":last3_4digits,"
                           ":sale_remarks,"
                           ":sale_customer_id,"
                           ":product_id"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":selling_price", selling_price);
    m_qry.bindValue(":date_sold", date_sold);
    m_qry.bindValue(":sale_paymentmethod", sale_paymentmethod);
    m_qry.bindValue(":sale_cheque_no", sale_cheque_no);
    m_qry.bindValue(":last3_4digits", last3_4digits);
    m_qry.bindValue(":sale_remarks", sale_remarks);
    m_qry.bindValue(":sale_customer_id", sale_customer_id);
    m_qry.bindValue(":product_id", product_id);

    if (m_qry.exec())
    {
        qDebug()<<"Sale transaction data added!";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createSaleInvoiceTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS saleInvoiceTable ("
                           "saleinvoice_id INTEGER PRIMARY KEY,"
                           "date TEXT NOT NULL"
                           ");";

    m_qry.prepare(sqlStatement);
    if (m_qry.exec())
    {
        qDebug()<<"Sale invoice table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addSaleInvoice(const QString &date)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO saleInvoiceTable ("
                           "date"
                           ")"
                           "VALUES ("
                           ":date"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":date", date);
    if (m_qry.exec())
    {
        qDebug()<<"Sale invoice data added";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createSaleTxnInvoiceTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS saleTxnInvoiceTable ("
                           "saletxn_id INTEGER PRIMARY KEY,"
                           "saleinvoice_id INTEGER,"
                           "FOREIGN KEY (saleinvoice_id) REFERENCES saleInvoiceTable (saleinvoice_id)"
                           ");";

    m_qry.prepare(sqlStatement);
    if (m_qry.exec())
    {
        qDebug()<<"Sale transaction/invoice bridging table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addSaleTxnInvoice(const int& saleinvoice_id)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO saleTxnInvoiceTable ("
                           "saleinvoice_id"
                           ")"
                           "VALUES ("
                           ":saleinvoice_id"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":saleinvoice_id", saleinvoice_id);

    if (m_qry.exec())
    {
        qDebug()<<"Sale bridging table updated";
    }
    else {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createConsignmentTxnTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS consignmentTxnTable ("
                           "consignmenttxn_id INTEGER PRIMARY KEY,"
                           "hrc_id TEXT NOT NULL,"
                           "product_id INTEGER NOT NULL,"
                           "hrc_cost TEXT NOT NULL,"
                           "date_consigned TEXT NOT NULL,"
                           "FOREIGN KEY (product_id) REFERENCES productTable (product_id)"
                           ");";

    m_qry.prepare(sqlStatement);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment transaction table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addConsignmentTxn(const QString& hrc_id, const int& product_id,
                                  const QString& hrc_cost, const QString& date_consigned)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO consignmentTxnTable ("
                           "hrc_id,"
                           "product_id,"
                           "hrc_cost,"
                           "date_consigned"
                           ")"
                           "VALUES ("
                           ":hrc_id,"
                           ":product_id,"
                           ":hrc_cost,"
                           ":date_consigned"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":hrc_id", hrc_id);
    m_qry.bindValue(":product_id", product_id);
    m_qry.bindValue(":hrc_cost", hrc_cost);
    m_qry.bindValue(":date_consigned", date_consigned);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment Transaction recorded";
    }
    else {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createConsignmentInvoiceTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS consignmentInvoiceTable ("
                           "consignmentinvoice_id INTEGER PRIMARY KEY,"
                           "date_consigned TEXT NOT NULL"
                           ");";

    m_qry.prepare(sqlStatement);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment invoice table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addConsignmentInvoice(const QString& date_consigned)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO consignmentInvoiceTable ("
                           "date_consigned"
                           ")"
                           "VALUES ("
                           ":date_consigned"
                           ");";
    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":date_consigned", date_consigned);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment Invoice added";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createConsignmentTxnInvoiceTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS consignmentTxnInvoiceTable ("
                           "consignmenttxn_id INTEGER PRIMARY KEY,"
                           "consignmentinvoice_id INTEGER NOT NULL,"
                           "FOREIGN KEY (consignmentinvoice_id) REFERENCES consignmentinvoiceTable (consignmentinvoice_id)"
                           ");";

    m_qry.prepare(sqlStatement);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment transaction/invoice bridging table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addConsignmentTxnInvoice(const int& consignmentinvoice_id)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO consignmentTxnInvoiceTable ("
                           "consignmentinvoice_id"
                           ")"
                           "VALUES ("
                           ":consignmentinvoice_id"
                           ");";
    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":consignmentinvoice_id", consignmentinvoice_id);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment Txn/Invoice bridging added";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createConsignmentPaymentTxnTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS consignmentPaymentTxnTable ("
                           "consignmentpaymenttxn_id INTEGER PRIMARY KEY,"
                           "wh_cost_price REAL NOT NULL,"
                           "hrc_sale_invoice TEXT NOT NULL,"
                           "payment_cheque_no TEXT NOT NULL,"
                           "payment_date TEXT NOT NULL"
                           ");";

    m_qry.prepare(sqlStatement);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment payment transaction table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createStockOverviewTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS stockOverviewTable ("
                           "stock_id INTEGER PRIMARY KEY,"
                           "saletxn_id INTEGER,"
                           "purchasetxn_id INTEGER NOT NULL,"
                           "FOREIGN KEY (saletxn_id) REFERENCES saleTxnInvoiceTable (saletxn_id),"
                           "FOREIGN KEY (purchasetxn_id) REFERENCES purchaseTxnInvoiceTable (purchasetxn_id)"
                           ");";

    m_qry.prepare(sqlStatement);

    if (m_qry.exec())
    {
        qDebug()<<"Stock overview table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addPurchaseStockOverview(const int& purchasetxn_id)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO stockOverviewTable ("
                           "purchasetxn_id"
                           ")"
                           "VALUES ("
                           ":purchasetxn_id"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":purchasetxn_id", purchasetxn_id);

    if (m_qry.exec())
    {
        qDebug()<<"Stock overview updated";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addSaleStockOverview(const int& saletxn_id, const int& stock_id)
{
    QSqlQuery m_qry;
    QString sqlStatement = "UPDATE stockOverviewTable SET saletxn_id = :saletxn_id WHERE stock_id = :stock_id";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":saletxn_id", saletxn_id);
    m_qry.bindValue(":stock_id", stock_id);

    if (m_qry.exec())
    {
        qDebug()<<"Stock overview sales column updated";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addSaleConsignmentOverview(const int& saletxn_id, const int& consignment_id)
{
    QSqlQuery m_qry;
    QString sqlStatement = "UPDATE consignmentOverviewTable SET saletxn_id = :saletxn_id WHERE consignment_id = :consignment_id";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":saletxn_id", saletxn_id);
    m_qry.bindValue(":consignment_id", consignment_id);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment overview sales column updated";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::createConsignmentOverviewTable()
{
    QSqlQuery m_qry;
    QString sqlStatement = "CREATE TABLE IF NOT EXISTS consignmentOverviewTable ("
                           "consignment_id INTEGER PRIMARY KEY,"
                           "consignmenttxn_id INTEGER NOT NULL,"
                           "saletxn_id INTEGER,"
                           "consignmentpaymenttxn_id INTEGER,"
                           "FOREIGN KEY (consignmenttxn_id) REFERENCES consignmentTxnTable (consignmenttxn_id),"
                           "FOREIGN KEY (saletxn_id) REFERENCES saleTxnInvoiceTable (saletxn_id),"
                           "FOREIGN KEY (consignmentpaymenttxn_id) REFERENCES consignmentpaymenttxnTable (consignmentpaymenttxn_id)"
                           ");";

    m_qry.prepare(sqlStatement);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment overview table connected";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

void dbManager::addConsignmentOverview(const int& consignmenttxn_id)
{
    QSqlQuery m_qry;
    QString sqlStatement = "INSERT INTO consignmentOverviewTable ("
                           "consignmenttxn_id"
                           ")"
                           "VALUES ("
                           ":consignmenttxn_id"
                           ");";

    m_qry.prepare(sqlStatement);
    m_qry.bindValue(":consignmenttxn_id", consignmenttxn_id);

    if (m_qry.exec())
    {
        qDebug()<<"Consignment overview updated";
    }
    else
    {
        qDebug()<<m_qry.lastError();
    }
}

